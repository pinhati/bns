@php
    use App\Http\Controllers\Server\BannedController;
    use App\Http\Controllers\Server\PlayerController;

    $playerInfo = PlayerController::getPlayerInfo($info->idBanned, ['nome', 'skin']);

    $banido = $playerInfo->nome;
    $admin = $info->adminBan;
    $motivo = $info->reasonBan;
    $data = $info->dateBan;

    $exp = explode('.', $info->ipBan);

    $asterisk = [];
    $asterisk[0] = str_repeat("*", strlen($exp[1]));
    $asterisk[1] = str_repeat("*", strlen($exp[2]));

    $ip = $exp[0].'.'.$asterisk[0].'.'.$asterisk[1].'.'.$exp[3];

    $adminInfo = PlayerController::getPlayerInfo($info->adminBan, ['nome', 'skin']);

@endphp


@extends('adminlte::page')

@section('title', 'Dashboard - Brasil New StarT')

@section('content_header')
    <h1>Revisão</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title">Jogador</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                          <div class="product-img">
                            <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Avatar_{{ $playerInfo->skin }}.png" alt="Product Image" class="direct-chat-img">
                          </div>
                          <div class="product-info mt-2">
                            <span style="color:#fff ;text-shadow: 0px 0px 2px #fff;font-size: 20px;">
                                {{ $banido }}
                            </span>
                          </div>
                        </li>
                        <!-- /.item -->
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>

            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title">Administrador</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                            <div class="product-img">
                              <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Avatar_{{ $adminInfo->skin }}.png" alt="Product Image" class="img-size-50 direct-chat-img">
                            </div>
                            <div class="product-info">
                              <span style="color:#ff00f4 ;text-shadow: 0px 0px 2px #ff00f4;font-size: 20px;">
                                {{ $admin }}
                              </span>
                              <span class="product-description mt-n1">
                                <span style="color:#ff00f4 ;text-shadow: 0px 0px 2px #ff00f4">Administrador</span>
                              </span>
                            </div>
                          </li>
                        <!-- /.item -->
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

        <div class="col-md-9">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-user-alt-slash mr-2 fa-xs"></i>{{ $banido }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    Olá, fui banido no dia <code class="ml-1 mr-1">{{ date('d/m/Y H:i:s', strtotime($data)) }}</code> pelo administrador <code class="ml-1">{{ $admin }}</code>, no qual a razão foi <code class="ml-1">{{ $motivo }}</code>.
                </div>
                <!-- /.card-body -->
            </div>
            <!-- START THE COMMENTS -->
            @foreach ($comments as $value)
            @php
                $playerName = PlayerController::getPlayerInfo($value->userID, ['nome'])
            @endphp
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="far fa-user mr-2 fa-xs"></i>{{ $playerName->nome }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {{ $value->text }}
                </div>
                <!-- /.card-body -->
            </div>

            @endforeach
            <!-- /.card -->
          <form action="/comment/new/1/{{ $info->token }}" method="post">
            @csrf
            <div class="input-group">
              <input type="text" name="text" placeholder="Type Message ..." class="form-control">
              <span class="input-group-append">
                <button type="submit" class="btn btn-primary">Send</button>
              </span>
            </div>
          </form>
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
