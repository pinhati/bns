@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Administrativo - LOG's</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Log Chat</h3>
                <div class="card-tools">
                    <a type="button" class="btn btn-tool" data-toggle="modal" href="#executeSQL">
                        <i class="fas fa-server"></i>
                    </a>
                    <a type="button" class="btn btn-tool" data-toggle="modal" href="#filter">
                        <i class="fas fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm table-responsible" id="tbl_logAll">
                     <thead>
                        <tr>
                            @foreach($log[0] as $key => $value)
                                <th scope="col">{{ $key }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <!--<tbody>
                        @foreach($log as $key => $value)
                            <tr>
                                @foreach($log[0] as $keyColumn => $valueColumn)
                                    <td scope="row">{{ $value->{$keyColumn} }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="filter" style="display: none;" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <form method="post" action="/admin/logs">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filtrar</h4>
                </div>
                <div class="modal-body">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Coluna</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Operador</th>
                                <th scope="col">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php

                                $structure = DB::connection('bnsCon')->select('describe logs');

                            @endphp
                            @foreach($structure as $key => $value)
                                <tr>
                                    <input type="hidden" value="{{ $value->Field }}" name="col[{{ $key }}]">
                                    @if($value->Key)
                                        @if($value->Extra == 'auto_increment')
                                            <td>{{ $value->Field }}<i class="fas fa-key text-warning ml-2" title="PRIMARY KEY"></i></td>
                                        @else
                                            <td>{{ $value->Field }}<i class="fas fa-key text-gray ml-2" title="ÍNDICE"></i></td>
                                        @endif
                                    @else
                                        <td>{{ $value->Field }}</td>
                                    @endif
                                    <td>{{ $value->Type }}</td>
                                    <td>
                                        <select style="border: none !important;" id="ColumnOperator{{ $key }}" class="form-control" name="operator[{{ $key }}]" width="100%">
                                            <option value="=">Igual (=)</option>
                                            <option value=">">Maior (>)</option>
                                            <option value=">=">Maior ou igual (&gt;=)</option>
                                            <option value="<">Menor (&lt;)</option>
                                            <option value="<=">Menor ou igual (&lt;=)</option>
                                            <option value="!=">Diferente (!=)</option>
                                            <option value="LIKE" @if (strpos($value->Type, 'varchar') !== false) {{ 'selected' }} @endif>Contém (LIKE)</option>
                                            <option value="NOT LIKE">Não Contém (NOT LIKE)</option>
                                        </select>
                                    </td>
                                    <td><input type="text" name="criterio[{{ $key }}]" class="form-control" style="width: 100%; border: none !important;" value=""></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="executeSQL" style="display: none;" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <form method="post" action="/admin/logs">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Executar SQL</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="sql" class="form-control" style="width: 100%; border: none !important;" value="{{ $requests->sql }}">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script>
//ajax populate with data
    $(document).ready(function() {
        var table = $('#tbl_logAll').DataTable({
            "aaSorting": [
                [0, 'desc'],
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
            },
            "searching": false,
            
            processing: true,
            serverSide: true,
            rowId: '#',
            ajax: {
                url: '/admin/logs/data?{!! http_build_query(app("request")->all()) !!}',
                type: 'get'
            },
            columns: [
                { data: 'id', name: 'logs.id' },
                { data: 'nomelog', name: 'logs.nomelog' },
                { data: 'log_texto', name: 'logs.log_texto' },
                { data: 'data', name: 'logs.data' },  
            ]
        });
    })
</script>
@stop