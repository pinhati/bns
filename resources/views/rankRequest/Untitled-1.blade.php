<div class="col-md-3">
    <div class="card card-outline">
        <div class="card-header">
            <h3 class="card-title">Autor</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <ul class="products-list product-list-in-card pl-2 pr-2">
                <li class="item">
                  <div class="product-img">
                    <img class="profile-user-img img-fluid img-circle bg-gray p-0"  src="https://wcode.ro/demo/lara/app-assets/images/avatars/Skin_{{ $playerInfo->skin }}.png" alt="User profile picture">
                  </div>
                  <div class="product-info mt-2">
                    <span style="color:#fff ;text-shadow: 0px 0px 2px #fff;font-size: 20px;">
                        {{ $author }}
                    </span>
                  </div>
                </li>
                <!-- /.item -->
            </ul>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    @if ($info->status==0)
        <div class="card card-outline">
            <div class="card-header">
                <h3 class="card-title">Moderação</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body text-center">
                <div class="input-group-prepend" style="display: block;">
                    <button type="button" class="btn btn-primary pl-4 pr-4 dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    Ações
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">

                        @if ($info->status==0)
                            <a class="dropdown-item" href="/denunciations/setStatus/{{ $info->token }}/2">Aprovar requisição</a>
                            <a class="dropdown-item" href="/denunciations/setStatus/{{ $info->token }}/3">Reprovar requisição</a>
                        @endif


                        @if ($info->locked==0)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/denunciations/lockProcess/{{ $info->token }}/1">Trancar requisição</a>
                        @else
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/denunciations/lockProcess/{{ $info->token }}/0">Destrancar requisição</a>
                        @endif


                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    @endif
    <!-- /.card -->
</div>