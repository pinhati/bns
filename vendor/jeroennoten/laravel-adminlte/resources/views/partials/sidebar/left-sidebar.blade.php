<?php
    use App\Http\Controllers\Panel\PermissionController;
    $getAdmin = DB::table('admins')->where('userID', Auth::user()->id)->pluck('permID');
    $getPerm = PermissionController::checkPermIn($getAdmin)->toArray();
?>
<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    @if(config('adminlte.logo_img_xl'))
        @include('adminlte::partials.common.brand-logo-xl')
    @else
        @include('adminlte::partials.common.brand-logo-xs')
    @endif

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="pt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('adminlte.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if(config('adminlte.sidebar_nav_animation_speed') != 300)
                    data-animation-speed="{{ config('adminlte.sidebar_nav_animation_speed') }}"
                @endif
                @if(!config('adminlte.sidebar_nav_accordion'))
                    data-accordion="false"
                @endif>

                <?php
                    $sideItems['withoutTitles'] =
                    DB::table('sidebar_items')
                    ->where('treeviewID', null)
                    ->where('groupID', null)
                    ->groupBy('sidebar_items.id')->get(['sidebar_items.*']);
                    foreach($sideItems['withoutTitles'] as $keyItems => $valueItems){
                        if($valueItems->permRequired == null){
                            $checkPerm = true;
                        }else{
                            $checkPerm = PermissionController::searchInArray($getPerm, $valueItems->permRequired);
                        }
                        if($checkPerm == true){
                            if($valueItems->isTreeview == true){
                                echo '
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon '.$valueItems->icon.' mr-2"></i>
                                        <p>
                                            '.$valueItems->name.'
                                            <i class="fas fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">';
                                        $sideItems['withTreeview'] =
                                        DB::table('sidebar_items')
                                        ->where('treeviewID', $valueItems->id)
                                        ->where('groupID', null)
                                        ->groupBy('sidebar_items.id')->get(['sidebar_items.*']);
                                        foreach($sideItems['withTreeview'] as $keyTreeview => $valueTreeview){
                                            if($valueTreeview->permRequired == null){
                                                $checkPerm = true;
                                            }else{
                                                $checkPerm = PermissionController::searchInArray($getPerm, $valueTreeview->permRequired);
                                            }
                                            if($checkPerm == true){
                                                echo '
                                                <li class="nav-item">
                                                    <a href="'.$valueTreeview->url.'" class="nav-link">
                                                    <i class="'.$valueTreeview->icon.' mr-2 nav-icon"></i>
                                                        <p>'.$valueTreeview->name.'</p>
                                                    </a>
                                                </li>';
                                            }
                                        }
                                    echo '
                                    </ul>
                                </li>';
                            }else{
                                echo '
                                    <li class="nav-item">
                                        <a class="nav-link" href="'.$valueItems->url.'">
                                            <i class="fa-fw '.$valueItems->icon.' mr-2"></i>
                                            <p>
                                                '.$valueItems->name.'
                                            </p>
                                        </a>
                                    </li>
                                ';
                            }
                        }
                    }
                    $sideTitles =
                    DB::table('sidebar_titles')->get(['sidebar_titles.*']);
                    foreach($sideTitles as $keyTitles => $valueTitles){
                        if($valueTitles->permRequired == null){
                            $checkPerm = true;
                        }else{
                            $checkPerm = PermissionController::searchInArray($getPerm, $valueTitles->permRequired);
                        }
                        if($checkPerm == true){
                            $sideItems['withTitles'] =
                            DB::table('sidebar_items')
                            ->where('treeviewID', null)
                            ->where('groupID', $valueTitles->id)
                            ->groupBy('sidebar_items.id')->get(['sidebar_items.*']);
                            
                            echo '<li class="nav-header">'.strtoupper($valueTitles->name).'</li>';
                            foreach($sideItems['withTitles'] as $keyItems => $valueItems){
                                if($valueItems->permRequired == null){
                                    $checkPerm = true;
                                }else{
                                    $checkPerm = PermissionController::searchInArray($getPerm, $valueItems->permRequired);
                                }
                                if($checkPerm == true){
                                    if($valueItems->isTreeview == true){
                                        echo '
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fa-fw '.$valueItems->icon.' mr-2"></i>
                                                <p>
                                                    '.$valueItems->name.'
                                                    <i class="fas fa-angle-left right"></i>
                                                </p>
                                            </a>
                                            <ul class="nav nav-treeview">';
                                                $sideItems['withTreeview'] =
                                                DB::table('sidebar_items')
                                                ->where('treeviewID', $valueItems->id)
                                                ->where('groupID', $valueTitles->id)
                                                ->groupBy('sidebar_items.id')->get(['sidebar_items.*']);

                                                foreach($sideItems['withTreeview'] as $keyTreeview => $valueTreeview){
                                                    if($valueTreeview->permRequired == null){
                                                        $checkPerm = true;
                                                    }else{
                                                        $checkPerm = PermissionController::searchInArray($getPerm, $valueTreeview->permRequired);
                                                    }
                                                    if($checkPerm == true){
                                                        echo '
                                                        <li class="nav-item">
                                                            <a href="'.$valueTreeview->url.'" class="nav-link">
                                                            <i class="fa-fw '.$valueTreeview->icon.' mr-2 nav-icon"></i>
                                                                <p>'.$valueTreeview->name.'</p>
                                                            </a>
                                                        </li>';
                                                    }
                                                }
                                            echo '
                                            </ul>
                                        </li>';
                                    }else{
                                        echo '
                                            <li class="nav-item">
                                                <a class="nav-link" href="'.$valueItems->url.'">
                                                    <i class="fa-fw '.$valueItems->icon.' mr-2"></i>
                                                    <p>
                                                        '.$valueItems->name.'
                                                    </p>
                                                </a>
                                            </li>
                                        ';
                                    }
                                }
                            }
                        }
                    }
                ?>
            </ul>
        </nav>
    </div>

</aside>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function(){
        var current = location.pathname;
        $('nav li a').each(function(){
            var $this = $(this);
            // if the current path is like this link, make it active
            if($this.attr('href').indexOf(current) !== -1){
                $this.addClass('active');
                if($this.parent().parent().hasClass('nav-treeview')){
                    $this.parent().parent().prev().addClass('active');
                    $this.parent().parent().parent().addClass('menu-open');
                }
            }
        })
    })
</script>
