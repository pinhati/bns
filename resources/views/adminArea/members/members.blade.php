@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Configuração do Menu</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Configuração do Menu</h3>
            </div>
            <div class="card-body p-0">
              <table class="table table-sm" id="tbl_logChat">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nick</th>
                    <th scope="col">Cargo</th>
                    <th scope="col">Ações</th>
                  </tr>
                </thead>
              </table>
            </div>
        </div>
    </div>
</div>
@stop

