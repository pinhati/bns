@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Minhas Casas</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Minhas Casas</h3>
            </div>
            <div class="card-body p-0">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Jogador</th>
                            <th scope="col">Level</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Inquilino</th>
                            <th scope="col" width="200">IPTU</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($houses as $key => $value)
                            @php
                                $now = new DateTime('NOW');
                                $iptu = new DateTime($value->vencimento);
                                $interval = $now->diff($iptu);
                                if($iptu < $now){
                                    $infoIPTU = '<span class="badge bg-danger">VENCIDO</span>';
                                }else{
                                    $days = $interval->format('%a');
                                    $infoIPTU = '<span class="badge bg-primary">'.$days.' dias</span>';
                                }
                            @endphp
                            <tr>
                                <td scope="row">{{ $value->id }}</td>
                                <td title="PID: {{ $value->dono }}">{{ $value->dono_nome }}</td>
                                <td>{{ $value->level }}</td>
                                @switch($value->valor_tipo)
                                    @case(0)
                                        <td><span class="badge badge-success">Dinheiro</span></td>
                                        @break
                                    @case(1)
                                        <td><span class="badge badge-primary">Cash</span></td>
                                        @break

                                    @default

                                @endswitch
                                <td>{!! ($value->valor_tipo==0?'<b class="text-success">$</b>':'<b class="text-primary">C$</b>') !!} {{ number_format($value->valor,2,'.','.') }}</td>
                                <td>{{ $value->inquilino }}</td>
                                <td>{!! $infoIPTU !!}</td>
                                <td><div class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Ações
                                    </button>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item text-sm" href="http://localhost/pain/map.php?x={{ $value->entrada_x }}&y={{ $value->entrada_y }}">Visualizar no mapa</a>
                                    </div>
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

