<?php  
use App\Models\Panel\UserPanel;
use Illuminate\Support\Facades\Auth;

class UserInfo{
    public function user(){
        $user = UserPanel::where('userID', Auth::user()->id)->first();
        return $user;
    }

    public function externalUser($id){
        $user = UserPanel::where('userID', $id)->first();
        return $user;
    }
}