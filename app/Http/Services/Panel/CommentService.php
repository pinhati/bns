<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\Comment;
use Exception;

class CommentService {
    
    public function create(int $userID, int $areaID, string $topicID, string $text) : bool
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if($userID = 0) $ip = 'ROBOT';
        $comment = new Comment;
        $comment->userID = $userID;
        $comment->type = $areaID;
        $comment->topicID = $topicID;
        $comment->text = $text;
        $comment->ip_address = $ip;
        $saved = $comment->save();

        if(!$saved) throw new Exception("Erro na requisição 'CommentService->create'");

        return true;
    }

    public function show(int $areaID, string $topicID)
    {
        $comment = Comment::where('type', $areaID)->where('topicID', $topicID)->with('user')->get();
        return $comment;
    }

}