<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Server\PlayerController;
use App\Http\Controllers\Server\RoleController;
use App\Http\Services\Panel\AdminService;
use App\Http\Services\Panel\CommentService;
use App\Http\Services\Panel\NotificationService;
use App\Http\Services\Panel\PostContentService;
use App\Http\Services\Panel\RankRequestService;
use App\Http\Services\Server\MailService;
use App\Models\Panel\RankRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RankRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->areaID = 5; //5 = requisições de rank
    }

    public function new(){
        $ranks = DB::table('permissions')->get(['name', 'id']);
        return view('rankRequest.form')->with('ranks', $ranks);
    }

    public function create(Request $request)
    {
        try
        {
            $permissionController = new PermissionController;

            $validate = $this->validateRequest($request);

            if($validate == false)      return Redirect::home();

            $rank = $permissionController->getPerm($request->role, ['id', 'name', 'roleTypeID', 'roleLevel']);

            $rankReqToken = $this->createRankRequest($rank->id, $rank->name, $request->responsibles);
            $this->createPostContent($rankReqToken, $rank->name);
            $this->insertComments(
                $rankReqToken,
                '<div class="alert bg-warning"><span class="text-light fontsize-17"><i class="icon fas fa-clock"></i> Requisição está aguardando um responsável</span></div>'
            );
            
            if($rank->roleTypeID != null && $rank->roleLevel != null){
                $searchRole = $this->getRoleInfo($rank->roleTypeID, $rank->roleLevel);
                if($searchRole !== false){
                    $this->insertComments(
                        $rankReqToken,
                        '<p>Olá! Verificamos que você realmente é '.$rank->name.' e setamos seu cargo.</p>'
                    );
                    $this->insertComments(
                        $rankReqToken,
                        '<div class="alert bg-success"><span class="text-light fontsize-17"><i class="icon fas fa-gavel"></i> Requisição aceita.</span></div><div class="alert bg-primary"><span class="text-light fontsize-17"><i class="icon fas fa-check"></i> Requisição finalizada.</span></div>'
                    );
                    $this->updateAdminRole($rank->id);
                }
            }
            $this->mentionResponsibles($request->responsibles, $rankReqToken);
            return redirect('/RankRequest/RR-'.$rankReqToken);
        }
        catch(Exception $e)
        {
            echo $e;
            exit;
            return abort(500);
        }
    }

    public function validateRequest(object $request) : bool
    {
        $params = $request->all();
        $paramsValidation = [
            'role' => 'required|int:11',
            'responsibles' => 'array'
        ];
        $validate = Validator::make($params, $paramsValidation);

        if($validate->fails()) return false;
        return true;
    }

    public function getRoleInfo(int $roleTypeID, int $roleLevel)
    {
        $roleController = new RoleController;
        $searchRole = $roleController->getRole($roleTypeID, $roleLevel);
        
        if(!$searchRole) return false;
        return $searchRole;
    }

    public function createRankRequest(int $rankID, string $rankName, array $responsibles) : string
    {
        $rankRequestService = new RankRequestService;
        $rankReqToken = $rankRequestService->create(Auth::user()->id, $rankID, $rankName, $responsibles, 1);

        return $rankReqToken;
    }

    public function createPostContent(string $rankReqToken, string $rankName) : bool
    {
        $postContentService = new PostContentService;
        $postContentService->create(Auth::user()->id, $this->areaID, $rankReqToken, 'Olá, estou requisitando o cargo <code class="ml-1">'.$rankName.'</code>, na data <code class="ml-1">'.date('d/m/Y H:i:s').'</code>.');

        return true;
    }

    public function insertComments(string $rankReqToken, string $message) : bool
    {
        $commentService = new CommentService;
        $commentService->create(0, $this->areaID, $rankReqToken, $message);
        
        return true;
    }

    public function updateAdminRole(int $rankID) : bool
    {
        $adminService = new AdminService;
        $adminService->createOrUpdate(Auth::user()->id, $rankID);
        
        return true;
    }

    public function mentionResponsibles(array $responsibles, string $rankReqToken) : bool
    {
        $playerController = new PlayerController;
        $notificationService = new NotificationService;
        $mailService = new MailService;
        foreach(array_slice($responsibles, 0, 3) as $value){
            $playerName = $playerController->getPlayerInfo((int)$value, ['nome']);
            $notificationService->create($value, Auth::user()->nome.' te mencionou.', '/RankRequest/RR-'.$rankReqToken);
            $mailService->create('Painel - New Start', $playerName->nome, 'Olá, você foi mencionado no painel. Acesse o painel e veja suas notificações.');
        }
        return true;
    }

    public function view($token)
    {
        $postContentService = new PostContentService;
        $commentService = new CommentService;
        $postContent = $postContentService->show($this->areaID, $token);
        $comments = $commentService->show($this->areaID, $token);
        $requestInfo = RankRequest::where('token', $token)->first(['id', 'userID', 'permID', 'permName', 'locked', 'status', 'token', 'ip_address', 'created_at']);
        return view('rankRequest.home', [
            'postContent' => $postContent,
            'info' => $requestInfo,
            'comments' => $comments
        ]);
    }
}
