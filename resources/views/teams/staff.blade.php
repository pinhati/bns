@php
use App\Http\Controllers\Server\ActivityController;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>CGA - Controle Geral Administrativo</h1>
@stop

@section('content')
<div class="row">
    @foreach($cargos as $key => $value)
        @php

            switch($value->nivel){
                case 1:
                    $cargo = 'Estagiário';
                    $color = '#ccff66';
                    $shadow = '#ccff66';
                    break;
                case 2:
                    $cargo = 'Moderador';
                    $color = '#3498db';
                    $shadow = '#3498db';
                    break;
                case 3:
                    $cargo = 'Administrador';
                    $color = '#ff00f4';
                    $shadow = '#ff00f4';
                    break;
                case 4:
                    $cargo = 'Encarregado';
                    $color = '#941bc2';
                    $shadow = '#941bc2';
                    break;
                case 5:
                    $cargo = 'Supervisor';
                    $color = '#fb9b04';
                    $shadow = '#fb9b04';
                    break;
                case 1337:
                    $cargo = 'Gerente';
                    $color = '#457B3B';
                    $shadow = '#457B3B';
                    break;
                case 3000:
                    $cargo = 'Diretor';
                    $color = '#9c8371';
                    $shadow = '#9c8371';
                    break;
                case 3001:
                    $cargo = 'Dono';
                    $color = '#00C7E2';
                    $shadow = '#00C7E2';
                    break;
                case 3002:
                    $cargo = 'Desenvolvedor';
                    $color = '#0f33ac';
                    $shadow = '#061855';
                    break;
                case 3003:
                    $cargo = 'Desenvolvedor';
                    $color = '#0f33ac';
                    $shadow = '#061855';
                    break;
                case 3004:
                    $cargo = 'Desenvolvedor';
                    $color = '#0f33ac';
                    $shadow = '#061855';
                    break;
                case 3005:
                    $cargo = 'Fundador';
                    $color = '#dc3545';
                    $shadow = '#dc3545';
                    break;
                default:
                    $cargo = $value->nivel;
                    break;
            }
            $tipo = 'Staff';
            if($value->skin > 311){
                $skin = 0;
            }else{
                $skin = $value->skin;
            }

            $activity = ActivityController::getActivity($value->player, 7); //(player_ID, last x days)
            if($value->nivel >= 3001 && $tipo == 'Staff'){
                $status = 'gray';
                $title = '';
            }elseif($activity < 3600){
                $status = 'danger';
                $title = 'Atividade inferior a 1 hora';
            }elseif($activity >= 3600 && $activity < 50400){
                $status = 'warning';
                $title = 'Atividade inferior a 14 horas';
            }elseif($activity >= 50400){
                $status = 'success';
                $title = 'Atividade superior a 14 horas';
            }else{
                $status = 'indigo';
                $title = '';
            }
        @endphp
        <div class="col-4">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle bg-gray p-0" src="https://wcode.ro/demo/lara/app-assets/images/avatars/Skin_{{ $skin }}.png" alt="User profile picture">
                    </div>
                    <h3 class="profile-username text-center">{{ $value->nome }}</h3>
                    <p class="text-center text-bold" style="color:{{ $color }};text-shadow: 0px 0px 2px {{ $shadow }};">{{ $cargo }}
                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item text-center">
                            <span class=""><b>STATUS:</b> <i title="{{ $title }}" class="fas fa-circle fa-xs text-{{ $status }} mr-1 ml-2"></i></span>
                        </li>
                        <li class="list-group-item text-center functionDiv">
                            <span class="functionAdmin"><b>FUNÇÃO:</b> {{ $value->funcao }}</span>
                        </li>
                    </ul>
                    <a href="#" class="btn btn-primary btn-block"><b>Seguir</b></a>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    @endforeach
</div>
@stop

