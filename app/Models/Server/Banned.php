<?php

namespace App\Models\Server;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banned extends Model
{
    protected $connection = 'bnsCon';
    protected $table = 'banidos';
    use HasFactory;
}
