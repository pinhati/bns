<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\Panel\NotificationService;
use Exception;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function read(Request $request)
    {
        try{
            $notificationService = new NotificationService;
            $paramsValidation = [
                'userID' => 'required|int:11'
            ];
            $params = $request->all();
            $validate = Validator::make($params, $paramsValidation);

            if($validate->fails()) return redirect()->back()->withErrors([$validate->errors()->getMessages()]);
            if(Auth::user()->id == $request->userID){
                $notificationService->updateReaded(Auth::user()->id);
            }else{
                abort(403, 'Requisição negada');
            }
            
           return redirect()->back();
        }catch(Exception $e){
            $codeError = ($e->getCode()?$e->getCode():500);
            abort($codeError, $e->getMessage());
        }

    }
}
