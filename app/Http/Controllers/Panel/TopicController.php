<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Panel\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function create($areaID, $topicID, $topicTitleColor, $topicTitle, $topicSubTitle)
    {
        $topic = new Topic;
        $topic->areaID = $areaID;
        $topic->topicID = $topicID;
        $topic->topicTitleColor = $topicTitleColor;
        $topic->topicTitle = $topicTitle;
        $topic->topicSubTitle = $topicSubTitle;
        $topic->authorID = Auth::user()->id;
        $topic->lastUserID = Auth::user()->id;
        $topic->lastComment = now();
        $topic->save();
        return $topic->id; //new row id
    }

    public static function updateForNewComment($areaID, $topicID)
    {
        $topic = Topic::where('areaID', $areaID)->where('topicID', $topicID);
        $topic->update([
            'lastUserID' => Auth::user()->id,
            'lastComment' => now()
        ]);
    }

    public function delete($id)
    {
        $topic = Topic::find($id);
        $i = $topic->delete();

        return $i; //true or false
    }
}
