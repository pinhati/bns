<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use App\Models\Server\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getRole(int $roleID, int $roleLevel)
    {
        $role = Role::where('player', Auth::user()->id)->where('cargo', $roleID)->where('nivel', $roleLevel)->first();
        return $role;
    }
}
