<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlayerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getPlayerInfo($playerInfo, $info){
        if(is_int($playerInfo)){
            $player = DB::connection('bnsCon')->table('player')->where('id', $playerInfo)->first($info);
        }else{
            $player = DB::connection('bnsCon')->table('player')->where('nome', $playerInfo)->first($info);
        }
        return $player;
    }

    public static function getPlayerInfosJson($playerInfo){
        $player = DB::connection('bnsCon')->table('player')->where('id', $playerInfo)->first(['id', 'nome', 'skin', 'level', 'horas_jogadas']);
        $cargo = DB::connection('bnsCon')->table('cargos')->where('player', $player->id)->first(['cargo', 'nivel']);
        if($cargo->cargo == 1){
            switch($cargo->nivel){
                case 1:
                    $role = 'Estagiário';
                    break;
                case 2:
                    $role = 'Moderador';
                    break;
                case 3:
                    $role = 'Administrador';
                    break;
                case 4:
                    $role = 'Encarregado';
                    break;
                case 5:
                    $role = 'Supervisor';
                    break;
                case 1337:
                    $role = 'Gerente';
                    break;
                case 3000:
                    $role = 'Diretor';
                    break;
                case 3001:
                    $role = 'Dono';
                    break;
                case 3002:
                    $role = 'Programador';
                    break;
                case 3005:
                    $role = 'Fundador';
                    break;
                default:
                    $role = $cargo->nivel;
                    break;
            }
        }else{
            $role = 'Helper';
        }

        $returnInfos = ['nome' => $player->nome, 'admin_nivel' => $role, 'skin' => $player->skin, 'level' => $player->level, 'horas_jogadas' => $player->horas_jogadas];
        return json_encode($returnInfos);
    }

    public static function getPlayerListName(Request $request){
        $player = DB::connection('bnsCon')->table('player')->where('nome', 'LIKE', $request->info.'%')->limit(10)->get(['id', 'nome']);

        $result = [];
        foreach($player as $v){
            $result[] = array("id"=>$v->id, "text"=>$v->nome);
        }

        return json_encode($result);
    }

    public function getOnlinePlayers(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://ip.brasilnewstart.com.br:5452/getServerInfo',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);
        $body = json_decode($response);
        return $body;
        //dd($body->players);
        //$infoPlayer = array_search(Auth::user()->nome, array_column($body->players, 'name'));
    }

    public static function getEconomy(){
        $player = DB::connection('bnsCon')->table('player')->where('level', '>', 3)->where('banido',0)->select(
            DB::raw("SUM(cash) as cash"),
            DB::raw("SUM(ouros) as gold"),
            DB::raw("SUM(dinheiro + dinheiro_banco) as money")
        )->first();

        $e = (object)[
            'cash' => $player->cash,
            'gold' => $player->gold,
            'money' => $player->money
        ];
        return $e;

    }

    public function getHouses(){
        $houses = DB::connection('bnsCon')->table('casas')->where('dono', Auth::user()->id)->get(['id', 'dono', 'dono_nome', 'level', 'valor_tipo', 'valor', 'inquilino', 'vencimento', 'entrada_x', 'entrada_y']);
        return view('several.ownership.house', ['houses' =>$houses]);
    }

    public function getCommerces(){
        $commerces = DB::connection('bnsCon')->table('propriedades')->where('dono', Auth::user()->id)->get();
        return view('several.ownership.commerces', ['commerces' =>$commerces]);
    }

    public function getFarms(){
        $farms = DB::connection('bnsCon')->table('fazendas')->leftJoin('player', 'fazendas.dono', '=', 'player.id')->where('dono', Auth::user()->id)->get(['fazendas.id', 'dono', 'descricao', 'entradaX', 'entradaY', 'preco', 'cofre', 'produtos', 'produtosRequeridos', 'vencimento', 'player.nome']);
        return view('several.ownership.farms', ['farms' =>$farms]);
    }
}





