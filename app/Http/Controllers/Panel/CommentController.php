<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Server\PlayerController;
use App\Models\Panel\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\Panel\CommentService;
use App\Http\Services\Panel\ErrorLogService;
use Exception;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request, $areaID, $topicID)
    {
        try{
            $commentService = new CommentService;
            $commentService->create(Auth::user()->id, $areaID, $topicID, $request->text);
        }catch(Exception $e){
            $codeError = ($e->getCode()?$e->getCode():500);
            abort($codeError, $e->getMessage());
        }

        return redirect()->back();
    }

    public static function getComments($areaID, $topicID)
    {
        try{
            $commentService = new CommentService;
            $comment = $commentService->show($areaID, $topicID);
        }catch(Exception $e){
            $codeError = ($e->getCode()?$e->getCode():500);
            abort($codeError, $e->getMessage());
        }
        
        return $comment;
    }

    public function deleteComment($id, $info){
        $commentInfo = Comment::where('id', $id)->firstOrFail(['userID', 'type','topicID']);

        $username = PlayerController::getPlayerInfo($commentInfo->userID, ['nome']);

        $update = Comment::find($id);
        $update->hide = $info;
        $update->save();

        switch($info){
            case 0:
                $name = 'Aberto';
                break;
            case 1:
                $name = 'Trancado';
                break;
            default:
                $name = 'Indefinido';
                break;
        }

        switch($commentInfo->type){
            case 1:
                $topicName = 'Revisões';
                break;
            case 2:
                $topicName = 'Denúncias';
                break;
            case 3:
                $topicName = 'Bugs';
                break;
            case 4:
                $topicName = 'Sugestões';
                break;
            case 5:
                $topicName = 'Requisições de Rank';
                break;
            default:
                $topicName = 'Indefinido';
                break;
        }


        HomeController::addLogPanel($topicName, 'Ocultou o comentário de '.$username->nome.' da '.$topicName.' ('.$commentInfo->topicID.') | PARA STATUS: '.$name.' | (S.A:'.$info.')');

        return redirect()->back();
    }
}
