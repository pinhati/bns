<?php

namespace App\Models\Server;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $connection = 'bnsCon';
    protected $table = 'email';
    public $timestamps = false;
    use HasFactory;
}
