@php
    use App\Http\Controllers\Server\PlayerController;

    $victimInfo = PlayerController::getPlayerInfo($info->idVictim, ['nome', 'skin']);
    $playerInfo = PlayerController::getPlayerInfo($info->idAuthor, ['nome', 'skin']);

    $author = $playerInfo->nome;
    $victim = $victimInfo->nome;
    $reason = $info->reason;
    $date = $info->created_at;

@endphp


@extends('adminlte::page')

@section('title', 'Dashboard - Brasil New StarT')

@section('content_header')
    <h1>Denúncias</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title">Denunciante</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                          <div class="product-img">
                            <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Avatar_{{ $playerInfo->skin }}.png" alt="Product Image" class="direct-chat-img">
                          </div>
                          <div class="product-info mt-2">
                            <span style="color:#fff ;text-shadow: 0px 0px 2px #fff;font-size: 20px;">
                                {{ $author }}
                            </span>
                          </div>
                        </li>
                        <!-- /.item -->
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>

            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title">Acusado</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                            <div class="product-img">
                              <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Avatar_{{ $victimInfo->skin }}.png" alt="Product Image" class="img-size-50 direct-chat-img">
                            </div>
                            <div class="product-info mt-2">
                              <span style="color:#ff0000 ;font-size: 20px;">
                                {{ $victim }}
                              </span>
                            </div>
                          </li>
                        <!-- /.item -->
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title">Moderação</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body text-center">
                    <div class="input-group-prepend" style="display: block;">
                        <button type="button" class="btn btn-primary pl-4 pr-4 dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                          Ações
                        </button>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">

                            <a class="dropdown-item" href="/denunciations/setStatus/{{ $info->token }}/1">Assumir processo</a>
                            @if ($info->status==1)
                                <a class="dropdown-item" href="/denunciations/setStatus/{{ $info->token }}/2">Aprovar processo</a>
                                <a class="dropdown-item" href="/denunciations/setStatus/{{ $info->token }}/3">Reprovar processo</a>
                            @endif


                            @if ($info->locked==0)
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/denunciations/lockProcess/{{ $info->token }}/1">Trancar processo</a>
                            @else
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/denunciations/lockProcess/{{ $info->token }}/0">Destrancar processo</a>
                            @endif


                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

        <div class="col-md-9">
            <div class="card card-outline card-success">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-user-plus mr-2 fa-xs"></i>{{ $author }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    Olá, estou denunciando o jogador <code class="ml-1">{{ $victim }}</code>, na data <code class="ml-1">{{ date('d/m/Y H:i:s', strtotime($date)) }}</code>, pelo motivo: <code class="ml-1">{{ $reason }}</code>.
                </div>
                <!-- /.card-body -->
            </div>
            <!-- START THE COMMENTS -->
            @foreach ($comments as $value)
                @php
                    $playerName = PlayerController::getPlayerInfo($value->userID, ['nome']);
                    if ($value->userID==$info->idVictim){
                        $color = "danger";
                        $icon = "fas fa-user-times";
                    }elseif ($value->userID==$info->idAuthor){
                        $color ="success";
                        $icon = "fas fa-user-plus";
                    }elseif ($value->userID==$info->idJudge){
                        $color = "fuchsia";
                        $icon = "fas fa-gavel";
                    }else{
                        $color = "dark";
                        $icon = "";
                    }

                @endphp
                @if ($value->hide==0)
                    <div class="card card-outline card-{{ $color }}">
                        <div class="card-header">
                            <h3 class="card-title"><i class="{{ $icon }} mr-2 fa-xs"></i>{{ $playerName->nome }}</h3>

                            <div class="card-tools">
                                <a type="button" class="btn btn-tool text-danger" href="/comment/deleteComment/{{ $value->id }}/1">
                                    <i class="fas fa-trash"></i>
                                </a>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! $value->text !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                @elseif ($value->hide==1)
                    @php
                        /*
                        <div class="card card-outline card-{{ $color }}">
                            <div class="card-header">
                                <h3 class="card-title"><i class="{{ $icon }} mr-2 fa-xs"></i>{{ $playerName->nome }}</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool text-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                {!! $value->text !!}
                            </div>
                            <!-- /.card-body -->
                        </div>*/
                    @endphp
                @endif

            @endforeach
            <!-- /.card -->
            @if ($info->locked==0 AND $info->status==0 || $info->locked==0 AND $info->status == 1)
                <div class="card card-outline">
                    <!-- /.card-header -->
                    <form action="/comment/new/2/{{ $info->token }}" method="post">
                        @csrf
                        <div class="card-body p-0">
                            <div class="input-group">
                                <div class="col-12">
                                    <textarea id="summernote" style="display: none;" rows="20" name="text"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Enviar</button>
                        </div>
                    </form>
                    <!-- /.card-body -->
                </div>
            @endif

        </div>
    </div>
@stop

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $('#summernote').summernote({
          placeholder: 'Conte aqui com detalhes todo o ocorrido.',
          tabsize: 2,
          height: 150
        });
    </script>
@stop
