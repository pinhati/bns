<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function staff(){
        $stmt = DB::connection('bnsCon')->table('cargos')->leftJoin('player', 'cargos.player', '=', 'player.id')->where('cargos.player', '!=', null)->where('cargos.cargo', '=', 1)->orderBy('cargos.nivel','desc')->orderBy('player.nome','ASC');
        $result = $stmt->limit(100)->get(['cargos.*', 'player.nome', 'player.skin']);
        return view('teams.staff')->with('cargos', $result);
    }
}
