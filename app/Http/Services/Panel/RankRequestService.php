<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\RankRequest;
use Exception;

class RankRequestService {
    
    public function create(int $userID, int $permID, string $permName, array $responsibles = [], string $status) : string
    {
        $token = md5(uniqid(rand(999,999999999).time(), true));
        $ip = $_SERVER['REMOTE_ADDR'];

        $rankRequest = new RankRequest;
        $rankRequest->token = $token;
        $rankRequest->userID = $userID;
        $rankRequest->permID = $permID;
        $rankRequest->permName = $permName;
        $rankRequest->responsibles = implode(",", array_slice($responsibles, 0, 3));
        $rankRequest->status = $status;
        $rankRequest->ip_address = $ip;
        $saved = $rankRequest->save();

        if(!$saved) throw new Exception("Requisição sem sucesso devido um erro. [saved: ".$saved."]");

        return $token;
    }

    public function updateStatus(int $id, int $status) : bool
    {
        $rankRequest = RankRequest::find($id);
        $rankRequest->status = $status;
        $saved = $rankRequest->save();

        if(!$saved) throw new Exception("Requisição sem sucesso devido um erro. [saved: ".$saved."]");

        return true;
    }

}