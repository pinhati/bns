<?php

namespace App\Models\Server;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $connection = 'bnsCon';
    protected $table = 'player';
    use HasFactory;
}
