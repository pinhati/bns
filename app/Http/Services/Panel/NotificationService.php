<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\Notification;
use Exception;
use Illuminate\Support\Facades\Auth;
use Mockery\Matcher\Not;

class NotificationService {
    
    public function create(int $userID, string $text, string $redirectLink) : bool
    {
        $notification = new Notification;
        $notification->userID = $userID;
        $notification->text = $text;
        $notification->read_time = null;
        $notification->redirect_to = $redirectLink;
        $saved = $notification->save();

        if(!$saved) throw new Exception("Requisição sem sucesso devido um erro. [saved: ".$saved."]");

        return true;
    }

    public function updateReaded(int $userID) : bool
    {

        Notification::where('userID', $userID)->update(['read_time' => now()]);

        return true;
    }

}