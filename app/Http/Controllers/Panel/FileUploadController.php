<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
//use App\Models\Panel\File;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function createForm(){
        return view('file-upload');
    }

    public function fileUpload(Request $req){

        //$fileModel = new File;

        if($req->file()) 
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

            //$fileModel->name = time().'_'.$req->file->getClientOriginalName();
            //$fileModel->file_path = '/storage/' . $filePath;
            //$fileModel->save();

            return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileName);
        }
    }
