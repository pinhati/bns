<?php

namespace App\Models\Panel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPanel extends Model
{
    use HasFactory;
    protected $fillable = [
        'userID',
    ];
    protected $table = 'users';
}
