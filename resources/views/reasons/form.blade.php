@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Novo motivo</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="/reason/new">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nome do motivo</label>
                <input type="text" class="form-control form-control-border" id="name" placeholder="Insira um motivo..." name="reason">
            </div>
            <div class="form-group">
                <label for="type">Áreas</label>
                <select required class="select2 custom-select form-control-border" multiple id="area" name="area[]">
                    <optgroup label="Administração">
                        @foreach($areas[0] as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </optgroup>
                    <optgroup label="Organizações">
                        @foreach($areas[1] as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="type">Tipo LOG</label>
                <select required class="custom-select form-control-border" id="type" name="type">
                    <option value="0">Sem LOG</option>
                    <option value="1">Com LOG</option>
                </select>
            </div>
            <div id="divlog" style="display: none">
                <div class="form-group">
                    <label for="name">Nome do LOG</label>
                    <input type="text" class="form-control form-control-border" id="logname" placeholder="Insira o nome do LOG..." name="namelog">
                </div>
                <div class="form-group">
                    <label for="name">Info. LOG</label>
                    <input type="text" value="%puniu #username#%" class="form-control form-control-border" id="searchloginfo" placeholder="Insira o nome do LOG..." name="searchloginfo">
                </div>
                <div class="form-group">
                    <label for="name">Posição do Admin</label>
                    <input type="number" value="0" class="form-control form-control-border" id="positionAdminName" placeholder="Insira o nome do LOG..." name="positionAdminName">
                </div>
            </div>
        </div>
      <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
</div>
@stop

@section('css')
    <style>
        .select2-selection{
            border-top: 0 !important;
            border-left: 0 !important;
            border-right: 0 !important;
            border-radius: 0 !important;
            box-shadow: inherit;
        }
    </style>
@stop

@section('js')
    <script>
    $('.select2').select2({
        width: '100%',
    });

    $( "#type" ).change(function() {
        if( $("#type").val() == 1){
            $("#divlog").show();
        }else{
            $("#divlog").hide();
        }


    });
    </script>
@stop
