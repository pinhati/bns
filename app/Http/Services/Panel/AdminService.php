<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\Admin;
use Exception;

class AdminService {
    
    public function getInfo(int $userID)
    {
        $admin = Admin::where('userID', $userID);
        if($admin->exists()){
            return $admin->first(['permID']);
        }else{
            return null;
        }
    }
    public function createOrUpdate(int $userID, int $permID) : bool
    {
        $saved = Admin::updateOrInsert(
            [
                'userID' => $userID,
                'permID' => $permID,
                'created_at' => now()
            ]
        );

        if(!$saved) throw new Exception("O admin não foi inserido na base de dados devido um erro.");

        return true;
    }

}