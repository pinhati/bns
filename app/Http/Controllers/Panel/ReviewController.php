<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Server\BannedController;
use App\Http\Controllers\Panel\CommentController;
use App\Models\Panel\Action;
use App\Models\Panel\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($token)
    {
        $reviewInfo = Review::where('token', $token)->firstOrFail(['id', 'idBanned', 'reasonBan', 'adminBan', 'ipBan', 'dateBan', 'token']);
        $comments = CommentController::getComments(1, $reviewInfo->token);
        return view('reviews.home')->with('info', $reviewInfo)->with('comments', $comments);
    }

    public function create(Request $request){
        $banInfo = BannedController::getBanInfo(Auth::user()->nome, ['admin', 'motivo']);

        if(!is_null($banInfo)){

            $token = md5(uniqid(rand(999,999999999).time(), true));

            $review = new Review;
            $review->token = $token;
            $review->idBanned = Auth::user()->id;
            $review->reasonBan = $banInfo->motivo;
            $review->adminBan = $banInfo->admin;
            $review->description = $request->description;
            $review->save();

            return redirect('/review/r'.$token);
        }else{
            return redirect()->back()->with('message', 'Você não está banido!');
        }
    }

    public function setStatus($token, $newStatus){
        $reviewInfo = Review::where('token', $token)->firstOrFail(['id']);
        $update = Review::find($reviewInfo->id);
        $update->status = $newStatus;
        $update->save();

        switch($newStatus){
            case 0:
                $name = 'Aguardando';
                break;
            case 1:
                $name = 'Em andamento';
                break;
            case 2:
                $name = 'Aprovado';
                break;
            case 3:
                $name = 'Reprovado';
                break;
            default:
                $name = 'Indefinido';
                break;
        }

        HomeController::addLogPanel('Revisões', 'Editou a revisão ('.$token.') | PARA STATUS: '.$name.' | (S.A:'.$newStatus.')');

        return redirect('/reviews/D-'.$token);
    }
}
