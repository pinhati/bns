@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Meus Comércios</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Meus Comércios</h3>
            </div>
            <div class="card-body p-0">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Jogador</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Cofre</th>
                            <th scope="col">Insumos</th>
                            <th scope="col">Insumos Req.</th>
                            <th scope="col" width="200">IPTU</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($commerces as $key => $value)
                            @php
                                 $prop_info = explode("|" ,$value->prop_info);
                                $nome = $prop_info[0];

                                $now = new DateTime('NOW');
                                $iptu = new DateTime($value->vencimento);
                                $interval = $now->diff($iptu);
                                if($iptu < $now){
                                    $infoIPTU = '<span class="badge bg-danger">VENCIDO</span>';
                                }else{
                                    $days = $interval->format('%a');
                                    $infoIPTU = '<span class="badge bg-primary">'.$days.' dias</span>';
                                }
                            @endphp
                            <tr>
                                <td scope="row">{{ $value->id }}</td>
                                <td title="PID: {{ $value->dono }}">{{ $nome }}</td>
                                <td><b class="text-success">$</b> {{ number_format($value->preco,0,'.','.') }}</td>
                                <td><b class="text-success">$</b> {{ number_format($value->cofre,0,'.','.') }}</td>
                                <td>{{ number_format($value->insumos,0,'.','.') }}</td>
                                <td>{{ number_format($value->insumosRequisitados,0,'.','.') }}</td>
                                <td>{!! $infoIPTU !!}</td>
                                <td><div class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Ações
                                    </button>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item text-sm" href="http://localhost/pain/map.php?x=&y=">Visualizar no mapa</a>
                                    </div>
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

