<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Server\PlayerController;
use App\Models\Panel\Action;
use App\Models\Panel\Comment;
use App\Models\Panel\Denunciation;
use App\Models\Panel\Evidence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DenunciationController extends Controller
{
    protected $areaID = 2;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($token)
    {
        $denunciationInfo = Denunciation::where('token', $token)->firstOrFail(['id', 'idAuthor', 'reason', 'idVictim', 'locked', 'status', 'token', 'created_at']);
        $comments = CommentController::getComments($this->areaID, $denunciationInfo->token);
        return view('denunciations.home')->with('info', $denunciationInfo)->with('comments', $comments);
    }

    public function new(){
        $areas = [];
        $areas[0] = DB::table('areas')->where('type', 0)->get(['name', 'id']);
        $areas[1] = DB::table('areas')->where('type', 1)->get(['name', 'id']);
        return view('denunciations.form')->with('areas', $areas);
    }

    public function create(Request $request){

        $token = md5(uniqid(rand(999,999999999).time(), true));
        $victimInfo = PlayerController::getPlayerInfo($request->nameVictim, ['id']);

        $denunciation = new Denunciation;
        $denunciation->token = $token;
        $denunciation->idAuthor = Auth::user()->id;
        $denunciation->orgAuthor = $request->orgAuthor;
        $denunciation->reason = $request->reason;
        $denunciation->idVictim = $victimInfo->id;
        $denunciation->orgVictim = $request->orgVictim;
        $denunciation->save();

        $comment = new Comment;
        $comment->userID = Auth::user()->id;
        $comment->type = $this->areaID;
        $comment->topicID = $token;
        $comment->text = $request->description;
        $comment->save();

        /*$evidence = new Evidence;
        $evidence->userID = Auth::user()->id;
        $evidence->type = $this->areaID;
        $evidence->topicID = $token;
        $evidence->route = '';
        $evidence->save();*/

        TopicController::create($this->areaID, $token, 'danger', 'Denúncia', $request->nameVictim.' ('.$request->orgVictim.')');

        return redirect('/denunciations/D-'.$token);
    }

    public function setStatus($token, $newStatus){
        $denunciantionInfo = Denunciation::where('token', $token)->firstOrFail(['id']);
        $update = Denunciation::find($denunciantionInfo->id);
        $update->status = $newStatus;
        $update->save();

        switch($newStatus){
            case 0:
                $name = 'Aguardando';
                break;
            case 1:
                $name = 'Em andamento';
                $commentText = '<div class="alert bg-primary"><span class="text-light fontsize-17"><i class="icon fas fa-gavel"></i> Um juiz assumiu este processo, aguarde um veredito.</span></div>';
                break;
            case 2:
                $name = 'Aprovado';
                $commentText = '<div class="alert bg-primary"><span class="text-light fontsize-17"><i class="icon fas fa-gavel"></i> O acusado será punido!</span></div>';
                $commentText .= '<div class="alert bg-green"><span class="text-light fontsize-17"><i class="icon fas fa-check"></i> Processo marcado como <b>Finalizado</b></span></div>';
                break;
            case 3:
                $name = 'Reprovado';
                $commentText = '<div class="alert bg-danger"><span class="text-light fontsize-17"><i class="icon fas fa-gavel"></i> Processo recusado pelo Juiz.</span></div>';
                $commentText .= '<div class="alert bg-green"><span class="text-light fontsize-17"><i class="icon fas fa-check"></i> Processo marcado como <b>Finalizado</b></span></div>';
                break;
            default:
                $name = 'Indefinido';
                break;
        }

        if(isset($commentText)){
            $comment = new Comment;
            $comment->userID = Auth::user()->id;
            $comment->type = $this->areaID;
            $comment->topicID = $token;
            $comment->text = $commentText;
            $comment->save();

            TopicController::updateForNewComment($this->areaID, $token);
        }


        HomeController::addLogPanel('Denúncias', 'Editou a denúncia ('.$token.') | PARA STATUS: '.$name.' | (S.A:'.$newStatus.')');

        return redirect('/denunciations/D-'.$token);
    }

    public function lockProcess($token, $info){
        $denunciantionInfo = Denunciation::where('token', $token)->firstOrFail(['id']);
        $update = Denunciation::find($denunciantionInfo->id);
        $update->locked = $info;
        $update->save();

        switch($info){
            case 0:
                $name = 'Aberto';
                break;
            case 1:
                $name = 'Trancado';
                break;
            default:
                $name = 'Indefinido';
                break;
        }


        HomeController::addLogPanel('Denúncias', 'Alterou os comentários da denúncia ('.$token.') | PARA STATUS: '.$name.' | (S.A:'.$info.')');

        return redirect('/denunciations/D-'.$token);
    }

    public function getLogsFromPunish(Request $request)
    {
        $reasons = DB::table('reasons')->where('id', $request->reason)->first(['type', 'namelog', 'searchLogInfo']);
        if($reasons->type == 1){

            $searchText = str_replace("#username#", Auth::user()->nome, $reasons->searchLogInfo);
            $logs = HomeController::viewLogServer($reasons->namelog, $searchText, ['id', 'data', 'log_texto']);
            return $logs;
        }else{
            return null;
        }
    }

    public function getAdminAndRole(Request $request)
    {
        $reasons = DB::table('reasons')->where('id', $request->reason)->first(['positionAdminName']);
        $logInfo = explode(": ", $request->punishInfo)[1];
        $logInfo = explode(" ", $logInfo)[$reasons->positionAdminName];
        $playerInfo = PlayerController::getPlayerInfo($logInfo, ['nome', 'admin_nivel', 'skin', 'level', 'horas_jogadas']);
        $cargo = DB::connection('bnsCon')->table('cargos')->where('player', $playerInfo->id)->first(['cargo', 'nivel']);
        if($cargo->cargo == 1){
            switch($cargo->nivel){
                case 1:
                    $role = 'Estagiário';
                    break;
                case 2:
                    $role = 'Moderador';
                    break;
                case 3:
                    $role = 'Administrador';
                    break;
                case 4:
                    $role = 'Encarregado';
                    break;
                case 5:
                    $role = 'Supervisor';
                    break;
                case 1337:
                    $role = 'Gerente';
                    break;
                case 3000:
                    $role = 'Diretor';
                    break;
                case 3001:
                    $role = 'Dono';
                    break;
                case 3002:
                    $role = 'Programador';
                    break;
                case 3005:
                    $role = 'Fundador';
                    break;
                default:
                    $role = $cargo->nivel;
                    break;
            }
        }else{
            $role = 'Helper';
        }

        $returnInfos = ['nome' => $playerInfo->nome, 'admin_nivel' => $role, 'skin' => $playerInfo->skin, 'level' => $playerInfo->level, 'horas_jogadas' => $playerInfo->horas_jogadas];
        return json_encode($returnInfos);
    }
}
