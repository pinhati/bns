<?php

namespace App\Models\Panel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RankRequest extends Model
{
    protected $table = 'rank_request';
    use HasFactory;
}
