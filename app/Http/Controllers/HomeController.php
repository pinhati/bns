<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Server\PlayerController;
use App\Models\Panel\Activity;
use App\Models\Panel\LogPanel;
use App\Http\Services\Server\MailService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\Panel\ErrorLogService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $feed = DB::table('feed')->orderBy('created_at', 'DESC')->limit(5)->get();

        $onlineUsers = DB::table('activity')->select(
            "activity.username",
            "activity.userID",
            "admins.permID"
        )->leftJoin("admins", "activity.userID", "=", "admins.userID")->where('activity.last_seen', ">=", now()->subMinutes(10))->groupBy("admins.userID")->get();

        $eco = DB::table('server_economy')->first();
        /*$minutesAgo = now()->subMinutes(60);

        if($minutesAgo >= $eco->updated_at){
            $eco = PlayerController::getEconomy();
            DB::table('server_economy')->where('id', 1)->update([
                'money' => $eco->money,
                'gold' => $eco->gold,
                'cash' => $eco->cash,
                'updated_at' => now()
            ]);
        }*/
        return view('home')
        ->with('skin', Auth::user()->skin)
        ->with('admins', $onlineUsers)
        ->with('feed', $feed)
        ->with('economy', $eco);
    }

    public static function addReactionFeed(Request $r)
    {
        $post = DB::table('feed_reactions')->updateOrInsert(['postID' => $r->postID, 'userID'=>Auth::user()->id], [
            'reactionName' => $r->reactionName,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $reactions = DB::table('feed_reactions')->where('postID', $r->postID)->groupBy('reactionName')->orderBy('count(reactionName)', 'DESC')->orderBy('reactionName', 'ASC')->limit(3)->selectRaw("reactionName, count(reactionName)")->get();
        $quantity = DB::table('feed_reactions')->where('postID', $r->postID)->count();

        $feed = DB::table('feed')->where('id', $r->postID)->update(['reactionsCount' => $quantity]);

        $arr = ['quantity' => $quantity, 'reactions' => $reactions];
        return json_encode($arr);
    }

    public static function getReactionsFeed($postID){

        $reactions = DB::table('feed_reactions')->where('postID', $postID)->groupBy('reactionName')->orderBy('count(reactionName)', 'DESC')->orderBy('reactionName', 'ASC')->limit(3)->selectRaw("reactionName, count(reactionName)")->get();
        return $reactions;
    }

    public static function checkIfReact($postID){

        $reactions = DB::table('feed_reactions')->where('postID', $postID)->where('userID', Auth::user()->id)->first(['reactionName']);
        return $reactions;
    }

    public static function addLogPanel($area, $action)
    {
        $log = new LogPanel;
        $log->userID = Auth::user()->id;
        $log->area = $area;
        $log->action = $action;
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
        return $log->id;
    }

    public static function addLogServer($area, $action)
    {
        $log = new LogPanel;
        $log->userID = Auth::user()->id;
        $log->area = $area;
        $log->action = $action;
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
        return $log->id;
    }

    public static function viewLogServer($nomelog, $log_texto, $columns)
    {

        $loginfo = DB::connection('bnsCon')->table('logs')
                     ->where('nomelog', $nomelog)
                     ->where('log_texto', 'LIKE', $log_texto)
                     ->whereDate('data', '>', Carbon::now()->subDays(3))
                     ->select($columns)
                     ->simplePaginate();
        return json_encode($loginfo);

    }

    public function sendMailServer(MailService $service, Request $request)
    {
        try{
            $paramsValidation = [
                'receiver' => 'required|string|min:3|max:255',
                'text' => 'required|string|min:1|max:255'
            ];
            $params = $request->all();
            $validate = Validator::make($params, $paramsValidation);

            if($validate->fails()) return redirect()->back()->withErrors([$validate->errors()->getMessages()]);

            $service->create(Auth::user()->nome, $request->receiver, $request->message);
            
        }catch(Exception $e){
            ErrorLogService::create(Auth::user()->id, Auth::user()->nome, $e->getMessage());
            abort(500);
        }

    }

    public static function getDiffDate($dateParam1, $dateParam2){
        $date1 = new \DateTime($dateParam1);
        $date2 = new \DateTime($dateParam2);
        $diffEdited = $date1->diff($date2);
        
        if($diffEdited->s < 60 && $diffEdited->i == 0 && $diffEdited->h == 0 && $diffEdited->d == 0 && $diffEdited->m == 0 && $diffEdited->y == 0){
            $textEdit = 'agora';
        }elseif($diffEdited->i >= 1 && $diffEdited->h == 0 && $diffEdited->d == 0 && $diffEdited->m == 0 && $diffEdited->y == 0){
            $textEdit = 'há '.$diffEdited->i.' '.($diffEdited->i==1?'minuto':'minutos');
        }elseif($diffEdited->h >= 1 && $diffEdited->d == 0 && $diffEdited->m == 0 && $diffEdited->y == 0){
            $textEdit = 'há '.$diffEdited->h.' '.($diffEdited->h==1?'hora':'horas');
        }elseif($diffEdited->d >= 1 && $diffEdited->d <= 3 && $diffEdited->m == 0 && $diffEdited->y == 0){
            $textEdit = 'há '.$diffEdited->d.' '.($diffEdited->d==1?'dia':'dias');
        }else{
            $textEdit = 'em '.date('d/m/Y H:i:s', strtotime($dateParam2));
        }

        return $textEdit;
    }

    public function getSkinBackground(){
        $images = [
            0 => [
                0 => [
                    'url' => 'https://i.pinimg.com/564x/3b/62/c3/3b62c3d5de89a518a723f0eb60c88e5d.jpg',
                    'css' => 'background-position: 26% 58%; background-size: 300%;'
                ],
                1 => [
                    'url' => 'https://www.mixmods.com.br/wp-content/uploads/2020/08/gta-sa-popcycle-noite-night-real-population-9912246.jpg',
                    'css' => 'background-position: 26% 58%; background-size: 300%;'
                ],
                2 => [
                    'url' => 'https://projetobatente.com.br/wp-content/uploads/2020/06/04-San-Fierro.png',
                    'css' => 'background-position: 26% 58%; background-size: 300%;'
                ],
                3 => [
                    'url' => 'https://www.mobilegta.net/downloads/picr/2021-01/1610047029_20210107_034442.jpg',
                    'css' => 'background-position: 26% 110%; background-size: 300%;'
                ],
            ],
            1 => [
                0 => [
                    'url' => 'https://i.pinimg.com/236x/c5/39/a0/c539a0c51e5158c41757f6c1742665e2.jpg',
                    'css' => 'background-position: 40% 0%;',
                ],
                1 => [ 
                    'url' => 'https://p4.wallpaperbetter.com/wallpaper/900/609/162/grand-theft-auto-grand-theft-auto-san-andreas-wallpaper-preview.jpg',
                    'css' => null
                ],
                3 => [
                    'url' => 'https://i.pinimg.com/originals/2b/6a/9b/2b6a9bd089b94e69906b63c96bca997f.jpg',
                    'css' => 'background-position: 10% 58%; background-size: 300%;'
                ],
                4 => [
                    'url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScdrLdAyMpSyOR74i1dOgv2R-rKWeKzrNYaA&usqp=CAU',
                    'css' => 'background-position: 70% 58%; background-size: 200%;'
                ]
            ]
        ];

        $avatar = null;
        if(date('H') > 5 && date('H') < 18){
            $type = 1;
            $avatar = array_rand($images[$type], 1);
        }else{
            $type = 0;
            $avatar = array_rand($images[$type], 1);
        }

        return ['rand' => $avatar, 'type' => $type, 'imageInfo' => $images];
    }

    public function getSkinImage($pathSearch, $skinID){
        
        $path = storage_path('app\\public\\panel\\'.$pathSearch.'\\Skin_'.$skinID.'.png');
        if (!File::exists($path)) {
            $path = storage_path('app\\public\\panel\\avatars\\Skin_0.png');
            if($pathSearch == 'avatars' || $pathSearch == 'skins')
                $path = storage_path('app\\public\\panel\\'.$pathSearch.'\\Skin_0.png');
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
