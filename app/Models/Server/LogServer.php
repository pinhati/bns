<?php

namespace App\Models\Server;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogServer extends Model
{
    protected $connection = 'bnsCon';
    protected $table = 'logs';
}
