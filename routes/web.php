<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/upload-file', [App\Http\Controllers\Panel\FileUploadController::class, 'createForm']);
Route::post('/upload-file', [App\Http\Controllers\Panel\FileUploadController::class, 'fileUpload'])->name('fileUpload');
Route::get('/t', function () {
    return view('adminArea.sidebar.config');
});

Auth::routes();

/* HOME */
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/serverInfo', [App\Http\Controllers\Server\PlayerController::class, 'getOnlinePlayers'])->name('home');
Route::get('/img/{path}/{id}', [App\Http\Controllers\HomeController::class, 'getSkinImage'])->name('getSkinImage');

/* FEED */
Route::post('/feed/addReaction', [App\Http\Controllers\HomeController::class, 'addReactionFeed'])->name('home');

/* REVIEWS */
Route::get('/reviews/D-{token}', [App\Http\Controllers\Panel\ReviewController::class, 'view'])->name('home');
Route::get('/reviews/setStatus/{token}/{status}', [App\Http\Controllers\Panel\ReviewController::class, 'setStatus'])->name('home');

/* DENUNCIATIONS */
Route::get('/denunciations/new', [App\Http\Controllers\Panel\DenunciationController::class, 'new'])->name('home');
Route::post('/denunciations/new', [App\Http\Controllers\Panel\DenunciationController::class, 'create'])->name('home');
Route::get('/denunciations/D-{token}', [App\Http\Controllers\Panel\DenunciationController::class, 'view'])->name('home');
Route::get('/denunciations/setStatus/{token}/{status}', [App\Http\Controllers\Panel\DenunciationController::class, 'setStatus'])->name('home');
Route::get('/denunciations/lockProcess/{token}/{info}', [App\Http\Controllers\Panel\DenunciationController::class, 'lockProcess'])->name('home');
Route::post('/denunciations/getPunishs', [App\Http\Controllers\Panel\DenunciationController::class, 'getLogsFromPunish'])->name('home');
Route::post('/denunciations/getVictimInfo', [App\Http\Controllers\Panel\DenunciationController::class, 'getAdminAndRole'])->name('home');

/* BUGS */
/* <--> */

/* SUGGESTIONS */
Route::get('/suggestions/new', [App\Http\Controllers\Panel\SuggestionController::class, 'new'])->name('home');
Route::post('/suggestions/new', [App\Http\Controllers\Panel\SuggestionController::class, 'create'])->name('home');


/* BUGS */
/* <--> */

/* RANK REQUESTS */
Route::GET('/RankRequest/RR-{token}', [App\Http\Controllers\Panel\RankRequestController::class, 'view'])->name('home');
Route::get('/RankRequest/new', [App\Http\Controllers\Panel\RankRequestController::class, 'new'])->name('home');
Route::post('/RankRequest/new', [App\Http\Controllers\Panel\RankRequestController::class, 'create'])->name('home');
/* <-----------> */

/* SEVERAL */
Route::get('/api/getPlayerJson', [App\Http\Controllers\Server\PlayerController::class, 'getPlayerListName'])->name('home');
Route::post('/api/getPlayerInfo/{id}', [App\Http\Controllers\Server\PlayerController::class, 'getPlayerInfosJson'])->name('home');
Route::get('/ownerships/houses', [App\Http\Controllers\Server\PlayerController::class, 'getHouses'])->name('home');
Route::get('/ownerships/commerces', [App\Http\Controllers\Server\PlayerController::class, 'getCommerces'])->name('home');
Route::get('/ownerships/farms', [App\Http\Controllers\Server\PlayerController::class, 'getFarms'])->name('home');



/* COMMENTS */
Route::post('/comment/new/{type}/{topicID}', [App\Http\Controllers\Panel\CommentController::class, 'create'])->name('home');
Route::get('/comment/deleteComment/{id}/{info}', [App\Http\Controllers\Panel\CommentController::class, 'deleteComment'])->name('home');

/* (ADM) REASONS */
Route::get('/reasons/new', [App\Http\Controllers\Panel\ReasonController::class, 'view'])->name('home');
Route::post('/reasons/new', [App\Http\Controllers\Panel\ReasonController::class, 'create'])->name('home');
Route::post('/reasons/getReasons', [App\Http\Controllers\Panel\ReasonController::class, 'getReasonsByArea'])->name('home');

/* (ADM) LOGS */

Route::get('/admin/logchat', [App\Http\Controllers\Server\LogController::class, 'viewLogChat'])->name('home');
Route::get('/admin/logchat/{x}/{y}/{date}', [App\Http\Controllers\Server\LogController::class, 'viewLogChatCoord'])->name('home');
Route::post('/admin/logchat', [App\Http\Controllers\Server\LogController::class, 'viewLogChat'])->name('home');
Route::get('/admin/logchat/data', [App\Http\Controllers\Server\LogController::class, 'logChatDATA'])->name('home');


Route::get('/admin/logs', [App\Http\Controllers\Server\LogController::class, 'viewLogAll'])->name('home');
Route::post('/admin/logs', [App\Http\Controllers\Server\LogController::class, 'viewLogAll'])->name('home');
Route::get('/admin/logs/data', [App\Http\Controllers\Server\LogController::class, 'logAllDATA'])->name('home');


Route::get('/admin/activity/staff', [App\Http\Controllers\Server\ActivityController::class, 'viewActivity'])->name('home');
Route::get('/admin/activity/staff/data', [App\Http\Controllers\Server\ActivityController::class, 'getTableData'])->name('home');


Route::get('/teams/staff', [App\Http\Controllers\Server\TeamController::class, 'staff'])->name('home');



Route::get('/sd', [App\Http\Controllers\Panel\NotificationController::class, 'read'])->name('home');


