<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTableData(){
        $currentDayYear = date('z', strtotime('+1 day'));
        $sevenDayYearAgo = date('z', strtotime('-7 days'));
        $stmt = DB::connection('bnsCon')->table('cargos')->select('cargos.*', 'player.nome', DB::raw('(SELECT SUM(atividades.atividade) FROM atividades WHERE atividades.player = cargos.player AND atividades.diaDoAno BETWEEN ? AND ?) as total_activity'))->setBindings([$sevenDayYearAgo, $currentDayYear])->leftJoin('player', 'cargos.player', '=', 'player.id')->where('cargos.player', '!=', null)->where('cargos.cargo', '<=', 2)->orderBy('cargos.cargo','ASC')->orderBy('cargos.nivel','desc')->orderBy('player.nome','ASC');
        return DataTables::of($stmt)->make(true);
    }

    public function viewActivity(Request $r){
        $currentDayYear = date('z', strtotime('+1 day'));
        $sevenDayYearAgo = date('z', strtotime('-7 days'));
        $stmt = DB::connection('bnsCon')->table('cargos')->select('cargos.*', 'player.nome', DB::raw('(SELECT SUM(atividades.atividade) FROM atividades WHERE atividades.player = cargos.player AND atividades.diaDoAno BETWEEN ? AND ?) as total_activity'))->setBindings([$sevenDayYearAgo, $currentDayYear])->leftJoin('player', 'cargos.player', '=', 'player.id')->where('cargos.player', '!=', null)->where('cargos.cargo', '<=', 2)->orderBy('cargos.cargo','ASC')->orderBy('cargos.nivel','desc')->orderBy('player.nome','ASC');
        $result = $stmt->limit(100)->get(['cargos.*', 'player.nome']);
        return view('adminArea.activity.home')->with('cargos', $result)->with('requests', $r);
    }

    public static function getActivity($playerID, $lastDays){
        $stmt = DB::connection('bnsCon')->table('atividades')->where('player', $playerID);
        $result = $stmt->limit($lastDays)->sum('atividade');
        return  $result;
    }
}
