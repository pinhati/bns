<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Panel\Reason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReasonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(){
        $areas = [];
        $areas[0] = DB::table('areas')->where('type', 0)->get(['name', 'id']);
        $areas[1] = DB::table('areas')->where('type', 1)->get(['name', 'id']);

        return view('reasons.form')->with('areas', $areas);
    }

    public static function getReasonsByArea(Request $r){
        $r = Reason::where('area', $r->area)->get(['id', 'name', 'area', 'type', 'namelog']);
        return json_encode($r);
    }

    public function create(Request $request){

        $reason = new Reason;
        $reason->userID = Auth::user()->id;
        $reason->name = $request->reason;
        $reason->type = $request->type;
        $reason->searchLogInfo = $request->searchloginfo;
        $reason->positionAdminName = $request->positionAdminName;
        if (isset($request->namelog)){
            $reason->namelog = $request->namelog;
        }
        $reason->save();

        return redirect()->back();
    }
}
