<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\ErrorLog;
use Exception;

class ErrorLogService {
    
    public static function create(int $userID, string $username, string $info) : bool
    {
        $errorLog = new ErrorLog();
        $errorLog->userID = $userID;
        $errorLog->info = $info;
        $saved = $errorLog->save();

        return true;
    }

}