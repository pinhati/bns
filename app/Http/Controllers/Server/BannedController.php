<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use App\Models\Server\Banned;

class BannedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getBanInfo($nome, $info){
        $banned = Banned::where('nome', $nome);
        if($banned->exists()){
            return $banned->first($info);
        }else{
            return null;
        }
    }
}
