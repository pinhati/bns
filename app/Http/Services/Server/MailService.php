<?php
namespace App\Http\Services\Server;

use App\Models\Server\Email;
use Exception;

class MailService {
    
    public function create($sender, $receiver, $text){
        $mail = new Email;
        $mail->de = $sender;
        $mail->para = $receiver;
        $mail->mensagem = $text;
        $mail->lido = 0;
        $mail->data = now();
        $saved = $mail->save();

        if(!$saved) throw new Exception("E-mail para ".$receiver." não foi inserido na base de dados devido um erro.");

        return true;
    }

}