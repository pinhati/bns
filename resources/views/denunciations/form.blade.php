@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Denunciar um Jogador</h3>
            </div>
            <div class="card-body p-0">
                <div class="bs-stepper linear">
                    <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                        <div class="step active" data-target="#logins-part">
                            <button type="button" class="step-trigger" role="tab" aria-controls="logins-part" id="logins-part-trigger" aria-selected="true">
                                <span class="bs-stepper-circle">1</span>
                                <span class="bs-stepper-label">Informações Iniciais</span>
                            </button>
                        </div>
                        <div class="line"></div>
                        <div class="step" data-target="#information-part">
                            <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                                <span class="bs-stepper-circle">2</span>
                                <span class="bs-stepper-label">Informações do Processo</span>
                            </button>
                        </div>
                        <div class="line"></div>
                        <div class="step" data-target="#descriptionInfo">
                            <button type="button" class="step-trigger" role="tab" aria-controls="descriptionInfo" id="descriptionInfo-trigger" aria-selected="false" disabled="disabled">
                                <span class="bs-stepper-circle">3</span>
                                <span class="bs-stepper-label">Descrição</span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                    <!-- your steps content here -->
                        <div id="logins-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="logins-part-trigger">
                            <div class="form-group">
                                <label for="areaSelector">Área do Processo</label>
                                <select required class="select2 custom-select form-control-border" id="areaSelector" name="reason">
                                    <option>Selecione</option>
                                    <optgroup label="Equipes">
                                        @foreach ($areas[0] as $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Organizações">
                                        @foreach ($areas[1] as $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="reasonSelector">Motivo</label>
                                <select required id="reasonSelector" class="select2 custom-select form-control-border" id="exampleSelectBorder" name="reason">
                                    <option disabled selected>Selecione</option>
                                </select>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="spinner-border" role="status" style="display: none;" id="spinnerPunish">
                                  <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div class="form-group" style="display: none; width: 100%;" id="formsdad">
                                <label for="exampleInputBorder">Qual punição você deseja apelar?</label>
                                <select required id="punishSelector" onchange="changePunishSelector()" class="select2 custom-select form-control-border" id="exampleSelectBorder" name="reason">
                                    <option>Selecione</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" id="next1">Próximo</button>
                        </div>
                        <div id="information-part" class="content" role="tabpanel" aria-labelledby="information-part-trigger">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="exampleInputBorder">Acusado</label>
                                        <input type="text" value="" disabled class="form-control" id="nameVictim" name="nameVictim" placeholder="Nome do acusado">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="exampleInputBorder">Cargo</label>
                                        <input type="text" value="" disabled class="form-control" id="orgVictim" name="orgVictim" placeholder="Nome do acusado">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="customFile">Provas</label>
                                        <div class="custom-file">
                                        <input type="file" class="custom-file-input  form-control-border" id="customFile" name="evidence">
                                        <label class="custom-file-label" for="customFile">Anexar provas</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" onclick="stepper.previous()">Anterior</button>
                            <button class="btn btn-primary" id="next2">Próximo</button>
                        </div>
                        <div id="descriptionInfo" class="content" role="tabpanel" aria-labelledby="descriptionInfo-trigger">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="summernote">Descrição</label>
                                        <textarea id="summernote" style="display: none;" rows="20" name="description"></textarea>
                                    </div>
                                </div>
                            <button class="btn btn-primary" onclick="stepper.previous()">Anterior</button>
                            <button type="submit" class="btn btn-primary">Próximo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4" style="display: none;" id="col2">
        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Usuário</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6 text-center">
                        <img src="" height="300" id="skinSearchInfo">
                    </div>
                    <div class="col-6">
                        <br>
                        <div class="form-group">
                            <label for="exampleInputBorder">Nick</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="nickSearchInfo" placeholder="Nick">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBorder">Level</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="levelSearchInfo" placeholder="Level">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBorder">Horas Jogadas</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="hoursSearchInfo" placeholder="Horas Jogadas">
                        </div>
                    </div>
                </div>
                <!-- /.d-flex -->
            </div>
        </div>
    </div>
</div>
@stop


@section('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>
    <script>
        $('#summernote').summernote({
          placeholder: 'Conte aqui com detalhes todo o ocorrido.',
          tabsize: 2,
          height: 150
        });
        var stepper = new Stepper(document.querySelector('.bs-stepper'));
        $('.select2').select2({
            width: '100%' // need to override the changed default
        });
        $( "#areaSelector" ).change(function() {
            const area = $(this).val();
            $("#reasonSelector").val(null).empty().trigger("change");
            $.ajax({
                url: '/reasons/getReasons',
                type: "post",
                data: {'_token': '{{ csrf_token() }}','area': area},
                dataType: 'JSON',
                success: function (returnValue) {
                    console.log(returnValue);
                    $.each(returnValue, function(k, v) {
                        $("#reasonSelector").append(`<option value="${v.id}" data-islog="${v.type}">${v.name}</option>`);
                    });
                }
            });
        });
        $( "#reasonSelector" ).change(function() {
            $("#formsdad").hide();
            $("#punishSelector").val(null).empty().trigger("change");

            var selected = $(this).find('option:selected');
            var islog = selected.data('islog');
            if(islog == 1){
                $("#spinnerPunish").show();
                const reason = $(this).val();
                $.ajax({
                    url: '/denunciations/getPunishs',
                    type: "post",
                    data: {'_token': '{{ csrf_token() }}','reason': reason},
                    dataType: 'JSON',
                    success: function (returnValue) {
                        var lenparse = returnValue.data.length;
                        if(lenparse > 0){
                            $.each(returnValue.data, function(k, v) {
                                $("#punishSelector").append(`<option value="[${v.data }] - ${v.log_texto}">[ ${v.data }] - ${v.log_texto}</option>`);
                            });

                            $("#spinnerPunish").hide();
                            $("#formsdad").show();
                            changePunishSelector();
                        }else{
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Erro',
                                body: 'Você não recebeu essa punição nos últimos 3 dias, logo não pode criar uma denúncia.'
                            })
                            $("#formsdad").show();
                            $("#spinnerPunish").hide();
                        }
                    }
                });
            }else{

            }
        });
        function changePunishSelector(){
            const punish = $("#punishSelector").val();
            const reason = $("#reasonSelector").val();
            $.ajax({
                url: '/denunciations/getVictimInfo',
                type: "post",
                data: {
                    '_token': '{{ csrf_token() }}',
                },
                dataType: 'JSON',
                success: function (returnValue) {
                    $("#nameVictim").val(returnValue.nome)
                    $("#orgVictim").val(returnValue.admin_nivel)
                    $("#skinSearchInfo").attr("src",`http://painel.brasilnewstart.com.br/dist/img/Skins/Skin_${returnValue.skin}.png`);
                    $("#nickSearchInfo").val(returnValue.nome);
                    $("#levelSearchInfo").val(returnValue.level);
                    $("#hoursSearchInfo").val(returnValue.horas_jogadas);

                    $("#col1").attr('class', 'col-md-12 col-lg-8');
                    $("#col2").show();
                }
            });
        }

        $( "#next1" ).click(function() {

            var selected = $("#reasonSelector").find('option:selected');
            var islog = selected.data('islog');

            if($("#areaSelector").val() && $("#reasonSelector").val()){
                if(islog == 1){
                    if($("#punishSelector").val()){
                        stepper.next();
                    }else{
                        $(document).Toasts('create', {
                            class: 'bg-danger',
                            title: 'Erro',
                            body: 'Preencha todas as informações necessárias'
                        })
                    }
                }else{
                    stepper.next();
                }
            }else{
                $(document).Toasts('create', {
                    class: 'bg-danger',
                    title: 'Erro',
                    body: 'Preencha todas as informações necessárias'
                })
            }
        });


        $( "#next2" ).click(function() {

            if($("#nameVictim").val() && $("#orgVictim").val()){
                stepper.next();
            }else{
                $(document).Toasts('create', {
                    class: 'bg-danger',
                    title: 'Erro',
                    body: 'Preencha todas as informações necessárias'
                })
            }
        });
    </script>
@stop
