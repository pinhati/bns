<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Services\Panel\AdminService;
use Exception;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAdminInfo($id)
    {
        try{
            $adminService = new AdminService;
            $adm = $adminService->getInfo($id);
        }catch(Exception $e){
            $codeError = ($e->getCode()?$e->getCode():500);
            abort($codeError, $e->getMessage());
        }

        return $adm;
    }
}