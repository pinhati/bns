<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;


class LogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewLogAll(Request $r){
        $result = DB::connection('bnsCon')->table('logs')->orderBy('id','desc')->limit(100)->get();
        return view('adminArea.logs.s_LogAll', [
            'log' => $result,
            'requests' => $r,
        ]);
    }

    public function logAllDATA(Request $r){
        $result = DB::connection('bnsCon')->table('logs');
        if($r->filled('criterio')){
            $stmt = DB::connection('bnsCon')->table('logs');
            foreach($r->criterio as $key => $value){
                if($value){
                    $stmt->where($r->col[$key], $r->operator[$key], $value);
                }
            }
            $result = $stmt;
        }else{
            $result = DB::connection('bnsCon')->table('logs')->orderBy('id','desc');
        }
        return DataTables::of($result)->make(true);
    }

    public function logChatDATA(Request $r){
        $stmt = DB::connection('bnsCon')->table('log_chat')->leftJoin('player', 'log_chat.player_id', '=', 'player.id')->select(['player.nome', 'log_chat.*']);
        if($r->filled('criterio')){
            foreach($r->criterio as $key => $value){
                if($value){
                    $stmt->where($r->col[$key], $r->operator[$key], $value);
                }
            }
        }
        if($r->filled('coordinatesX') && $r->filled('coordinatesY') && $r->filled('coordinatesDate')){
            $date = new DateTime();
            $date->setTimestamp($r->coordinatesDate);
            $dateInfo = $date->format('Y-m-d H:');
            $coord = [
                'x_sub' => $r->coordinatesX - 20,
                'y_sub' => $r->coordinatesY - 20,
                'x_add' => $r->coordinatesX + 20,
                'y_add' => $r->coordinatesY + 20,
                'date'  => $dateInfo
            ];
            $stmt -> where('pos_x', '>=', $coord['x_sub'])
                  -> where('pos_y', '>=', $coord['y_sub'])
                  -> where('pos_x', '<=', $coord['x_add'])
                  -> where('pos_y', '<=', $coord['y_add'])
                  -> where('datahora', 'LIKE', '%'.$coord['date'].'%');
        }
        return DataTables::of($stmt)->make(true);
    }

    public function viewLogChat(Request $r){
        $stmt = DB::connection('bnsCon')->table('log_chat')->leftJoin('player', 'log_chat.player_id', '=', 'player.id')->orderBy('id','desc');
        $result = $stmt->limit(100)->get(['log_chat.*', 'player.nome']);
        return view('adminArea.logs.s_Logchat', [
            'log' => $result,
            'requests' => $r,
        ]);
    }
}

