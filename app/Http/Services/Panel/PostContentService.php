<?php
namespace App\Http\Services\Panel;

use App\Models\Panel\PostContent;
use Exception;

class PostContentService {

    public function create(int $userID, int $areaID, string $postID, string $text) : bool
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $content = new PostContent;
        $content->userID = $userID;
        $content->type = $areaID;
        $content->postID = $postID;
        $content->text = $text;
        $content->ip_address = $ip;
        $saved = $content->save();
        
        if(!$saved) throw new Exception("A requisição não foi efetuada devido um erro.");

        return true;
    }

    public function show(int $areaID, string $topicID){
        $content = PostContent::where('type', $areaID)->where('postID', $topicID)->get(['userID', 'text', 'hide', 'ip_address', 'created_at', 'updated_at']);
        return $content;
    }
}