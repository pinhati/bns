<?php

namespace App\Http\Middleware;

use App\Models\Panel\Activity;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class UserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            $expiresAt = now()->addMinutes(10); /* keep online for 10 min */
            Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);

            /* last seen */
            $b = Activity::firstOrNew(['userID' => Auth::user()->id]);
            $b->username = Auth::user()->nome;
            $b->userID = Auth::user()->id;
            $b->last_seen = now();
            $b->save();
        }

        return $next($request);
    }
}
