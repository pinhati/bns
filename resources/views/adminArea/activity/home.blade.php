@php
use App\Http\Controllers\Server\ActivityController;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Administrativo - LOG's</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Log Chat</h3>
                <div class="card-tools">
                    <a type="button" class="btn btn-tool" data-toggle="modal" href="#filter">
                        <i class="fas fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm" id="tbl_activity">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Jogador</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Cargo</th>
                            <th scope="col">Função</th>
                            <th scope="col">Atividade (7d)</th>
                            <th scope="col">Salário</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                  <!--  <tbody>
                        @foreach($cargos as $key => $value)
                            @php

                                if($value->cargo == 1){
                                    switch($value->nivel){
                                        case 1:
                                            $cargo = 'Estagiário';
                                            break;
                                        case 2:
                                            $cargo = 'Moderador';
                                            break;
                                        case 3:
                                            $cargo = 'Administrador';
                                            break;
                                        case 4:
                                            $cargo = 'Encarregado';
                                            break;
                                        case 5:
                                            $cargo = 'Supervisor';
                                            break;
                                        case 1337:
                                            $cargo = 'Gerente';
                                            break;
                                        case 3000:
                                            $cargo = 'Diretor';
                                            break;
                                        case 3001:
                                            $cargo = 'Dono';
                                            break;
                                        case 3002:
                                            $cargo = 'Beta-Tester';
                                            break;
                                        case 3003:
                                            $cargo = 'Desenvolvedor';
                                            break;
                                        case 3005:
                                            $cargo = 'Fundador';
                                            break;
                                        default:
                                            $cargo = $value->nivel;
                                            break;
                                    }
                                    $tipo = 'Staff';
                                }else{
                                    $cargo = $value->nivel;
                                    $tipo = 'Helper';
                                }
                            @endphp
                            <tr>
                                <td scope="row">{{ $key }}.</td>
                                <td title="PID: {{ $value->player }}">{{ $value->nome }}</td>
                                <td>{{ $tipo }}</td>
                                <td>{{ $cargo }}</td>
                                <td>{{ $value->funcao }}</td>
                                <td>{{ gmdate('H:i:s', ActivityController::getActivity($value->player, 7)) }}</td>
                                <td>$ 50.000</td>
                                <td>
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Ações
                                        </button>
                                        <div class="dropdown-menu">
                                        <a class="dropdown-item text-sm" href="/admin/logchat">Descontar Salário</a>
                                        <a class="dropdown-item text-sm" href="http://localhost/pain/map.php?x=">Declarar Ausência</a>
                                        </div>
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="filter" style="display: none;" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <form method="post" action="/admin/logchat">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filtrar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Coluna</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Operador</th>
                                <th scope="col">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php

                                $structure = DB::connection('bnsCon')->select('describe log_chat');

                            @endphp
                            @foreach($structure as $key => $value)
                                <tr>
                                    <input type="hidden" value="{{ $value->Field }}" name="col[{{ $key }}]">
                                    @if($value->Key)
                                        @if($value->Extra == 'auto_increment')
                                            <td>{{ $value->Field }}<i class="fas fa-key text-warning ml-2" title="PRIMARY KEY"></i></td>
                                        @else
                                            <td>{{ $value->Field }}<i class="fas fa-key text-gray ml-2" title="ÍNDICE"></i></td>
                                        @endif
                                    @else
                                        <td>{{ $value->Field }}</td>
                                    @endif
                                    <td>{{ $value->Type }}</td>
                                    <td>
                                        <select style="border: none !important;" id="ColumnOperator{{ $key }}" class="form-control" name="operator[{{ $key }}]" width="100%">
                                            <option value="=">Igual (=)</option>
                                            <option value=">">Maior (>)</option>
                                            <option value=">=">Maior ou igual (&gt;=)</option>
                                            <option value="<">Menor (&lt;)</option>
                                            <option value="<=">Menor ou igual (&lt;=)</option>
                                            <option value="!=">Diferente (!=)</option>
                                            <option value="LIKE" @if (strpos($value->Type, 'varchar') !== false) {{ 'selected' }} @endif>Contém (LIKE)</option>
                                            <option value="NOT LIKE">Não Contém (NOT LIKE)</option>
                                        </select>
                                    </td>
                                    <td><input type="text" name="criterio[{{ $key }}]" class="form-control" style="width: 100%; border: none !important;" value=""></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script>
    //ajax populate with data
    $(document).ready(function() {
        var table = $('#tbl_activity').DataTable({
                "aaSorting": [
                    [2, "asc"],
                    [3, "desc"]
                    
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
                },

                processing: true,
                serverSide: true,
                rowId: '#',
                ajax: '{{ url('/admin/activity/staff/data') }}',
                columns: [
                    {
                        searchable: 'false',
                        render: function (data, type, row, meta) {
                            return  meta.row + 1 + '.';
                            
                        }
                    },
                    {
                        data: 'nome', name: 'player.nome',
                        
                        render: function (data, type, row, meta) {
                          
                           return data; 
                        }

                    },
                    {
                        data: 'cargo', name: 'cargos.cargo', 
                        render: function (data, type, row, meta) {
                            if(data == 1) {
                                return 'Staff';
                            }else{
                                return 'Helper';
                            }
                            
                        }
                    },
                    {
                        data: 'nivel', name: 'cargos.nivel',
                        render: function (data, type, row, meta) {
                            if(row['cargo'] == 1){
                                switch(data){
                                    case 1:
                                        return 'Estagiário';
                                        break;
                                    case 2:
                                        return 'Moderador';
                                        break;
                                    case 3:
                                        return 'Administrador';
                                        break;
                                    case 4:
                                        return 'Encarregado';
                                        break;
                                    case 5:
                                        return 'Supervisor';
                                        break;
                                    case 1337:
                                        return 'Gerente';
                                        break;
                                    case 3000:
                                        return 'Diretor';
                                        break;
                                    case 3001:
                                        return 'Dono';
                                        break;
                                    case 3002:
                                        return 'Beta-Tester';
                                        break;
                                    case 3003:
                                        return 'Desenvolvedor';
                                        break;
                                    case 3005:
                                        return 'Fundador';
                                        break;
                                    default:
                                        return  data;
                                        break;
                                    }
                            }else{
                                return data;
                            } 
                        }
                        
                    },
                    {
                        data: 'funcao', name:'cargos.funcao',
                    },
                    {
                        data: 'total_activity',
                        searchable: 'false',
                        render: function (data, type, row, meta) {
                            return new Date(data * 1000).toISOString().substr(11, 8);
                        }
                    },
                    {
                        searchable: 'false',
                        render: function (data, type, row, meta) {
                            return '$50.000';
                        }
                    },
                    {
                        searchable: 'false',
                        render: function(data, type, row, meta) {
                            return '<div class="input-group-prepend"> <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">  Ações </button> <div class="dropdown-menu"> <a class="dropdown-item text-sm" href="/admin/logchat">Descontar Salário</a> <a class="dropdown-item text-sm" href="http://localhost/pain/map.php?x=">Declarar Ausência</a> </div> </div>'
                        }
                    },
                ]

            });
    })
</script>
@stop


