@extends('adminlte::page')

@section('title', 'Requisitar Rank')

@section('content_header')
    <h1>Requisitar Rank</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Criar Requisição de Rank</h3>
            </div>
            <div class="card-body p-0">
                <div class="bs-stepper linear">
                    <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                        <div class="step active" data-target="#logins-part">
                            <button type="button" class="step-trigger" role="tab" aria-controls="logins-part" id="logins-part-trigger" aria-selected="true">
                                <span class="bs-stepper-circle">1</span>
                                <span class="bs-stepper-label">Informações Iniciais</span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                    <!-- your steps content here -->
                        <div id="logins-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="logins-part-trigger">
                            <form method="post" action="/RankRequest/new">
                                @csrf
                                <div class="form-group">
                                    <label for="areaSelector">Cargo</label>
                                    <select required class="select2 custom-select form-control-border" id="role_id_el" name="role">
                                        <option>Selecione</option>
                                        @foreach ($ranks as $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="responsibleName">Mencionar Responsáveis</label>
                                    <select required class="select2 custom-select form-control-border" id="responsible_id_el" name="responsibles[]" multiple>

                                    </select>
                                </div>
                                <button class="btn btn-primary" type="submit">Próximo</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4" style="display: none;" id="col2">
        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Usuário</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6 text-center">
                        <img src="" height="300" id="skinSearchInfo">
                    </div>
                    <div class="col-6">
                        <br>
                        <div class="form-group">
                            <label for="exampleInputBorder">Nick</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="nickSearchInfo" placeholder="Nick">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBorder">Level</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="levelSearchInfo" placeholder="Level">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBorder">Horas Jogadas</label>
                            <input type="text" value="" disabled class="form-control form-control-border" id="hoursSearchInfo" placeholder="Horas Jogadas">
                        </div>
                    </div>
                </div>
                <!-- /.d-flex -->
            </div>
        </div>
    </div>
</div>
@stop


@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>
    <script>
        var stepper = new Stepper(document.querySelector('.bs-stepper'));
        $('.select2').select2({
            width: '100%' // need to override the changed default
        });

        $(document).ready(function(){

            $("#responsible_id_el").select2({
                width: '100%',
                ajax: { 
                    url: "/api/getPlayerJson",
                    type: "get",
                    dataType: 'json',
                    delay: 450,
                    data: function (params) {
                        return {
                            info: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    }
                },
                placeholder: "Digite o nome do jogador (OBS: Letras maiúsculas e minúsculas interferem)",
                minimumInputLength: 3,
                cache: true
            });
            $("#responsible_id_el").on("change", function (e) {
                $.ajax({
                    url: '/api/getPlayerInfo/'+$("option:selected:last",this).val(),
                    type: "post",
                    data: {
                        '_token': '{{ csrf_token() }}',
                    },
                    dataType: 'JSON',
                    success: function (returnValue) {
                        $("#skinSearchInfo").attr("src",`http://painel.brasilnewstart.com.br/dist/img/Skins/Skin_${returnValue.skin}.png`);
                        $("#nickSearchInfo").val(returnValue.nome);
                        $("#levelSearchInfo").val(returnValue.level);
                        $("#hoursSearchInfo").val(returnValue.horas_jogadas);

                        $("#col1").attr('class', 'col-md-12 col-lg-8');
                        $("#col2").show();
                    }
                });
            });
        });
    </script>
@stop
