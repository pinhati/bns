@php
    use App\Http\Controllers\Server\PlayerController;
    use App\Http\Controllers\Panel\AdminController;
    use App\Http\Controllers\Panel\PermissionController;
    use App\Http\Controllers\HomeController;

    define('AREA_ID', 5);

    $playerInfo = PlayerController::getPlayerInfo($info->userID, ['nome', 'skin']);

    $author = $playerInfo->nome;
    $date = $info->created_at;

    /*https://i.pinimg.com/236x/c5/39/a0/c539a0c51e5158c41757f6c1742665e2.jpg*/
    /*https://i.pinimg.com/236x/92/d0/f5/92d0f578e644284a81f914447f37eedb.jpg*/
    /*https://i.pinimg.com/564x/3b/62/c3/3b62c3d5de89a518a723f0eb60c88e5d.jpg*/
    /*https://p4.wallpaperbetter.com/wallpaper/900/609/162/grand-theft-auto-grand-theft-auto-san-andreas-wallpaper-preview.jpg*/

@endphp


@extends('adminlte::page')

@section('title', 'Dashboard - Brasil New StarT')

@section('content_header')
    <h1>Requisição de Rank</h1>
@stop

@section('css')
@stop

@section('content')
    <div class="row">
        
        <div class="col-md-12">
            @foreach($postContent as $key => $value)
                @php
                    $playerName = PlayerController::getPlayerInfo($value->userID, ['nome']);

                    $adminInfo = AdminController::showAdminInfo($value->userID);
                    $roleInfo = PermissionController::getPerm($adminInfo->permID, ['name', 'color']);
                    $roleName = $roleInfo->name;
                    $roleImg = 'https://brasilnewstart.com.br/forum/uploads/monthly_2021_02/fundador.png.a0fd52da3037a16f15b784e9b0fbadc9.png';
                    $roleColor = $roleInfo->color;

                    $textCreated = HomeController::getDiffDate('now', $value->created_at);
                    if($value->created_at != $value->updated_at){
                        $textUpdated = HomeController::getDiffDate('now', $value->updated_at);
                    }else{
                        $textUpdated = null;
                    }

                    $avatar = HomeController::getSkinBackground();
                    $randNumber = $avatar['rand'];
                    $type = $avatar['type'];
                    $images = $avatar['imageInfo'];

                @endphp
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body cardBodyAll">
                        <div class="sideContentCardBodyPost">
                            <h5>
                                <strong>
                                    <a href="https://brasilnewstart.com.br/forum/profile/84-giooo/?wr=eyJhcHAiOiJmb3J1bXMiLCJtb2R1bGUiOiJmb3J1bXMtY29tbWVudCIsImlkXzEiOjcyNDcsImlkXzIiOjMzMzk3fQ==">
                                        <span style="color:{{ $roleColor }} ;text-shadow: 0px 0px 2px {{ $roleColor }}">
                                            <img src="" alt="">
                                            <span>{{ $playerName->nome }}</span>
                                        </span>
                                    </a>
                                </strong>
                            </h5>
                            <div class="product-img" style="margin-top: 20px">
                                <img class="profile-user-img img-fluid img-circle p-0" style="background-image: url({{ $images[$type][$randNumber]['url'] }}); {{ $images[$type][$randNumber]['css'] }}" src="/img/avatars/{{ $playerInfo->skin }}" alt="User profile picture">
                            </div>
                            <h6 style="margin-top: 10px;">
                                <span style="color:{{ $roleColor }} ;text-shadow: 0px 0px 2px {{ $roleColor }}">
                                    <img src="" alt="">
                                    <span>{{ $roleName }}</span>
                                </span>
                            </h6>
                            <img style="max-width: 100%" src="{{ $roleImg }}" alt="" class="cAuthorGroupIcon">
                            <span style="color: #B7B7B7">{{ UserInfo::user()->posts }} posts</span>
                        </div>
                        
                        <div class="mainContentCardHeaderPost">
                            <span>Postado {{ $textCreated }}</span>
                            @if($textUpdated != null)}
                                <span><span class="ml-2 mr-2">•</span> Editado {{ $textUpdated }}</span>
                            @endif
                            <span class="float-right">
                                <strong>IP:</strong> {{ $value->ip_address }}
                                <span><span class="ml-2 mr-2">•</span> <i class="fas fa-share"></i></span>
                                <span><span class="ml-2 mr-2">•</span> <i class="fas fa-flag"></i></span>
                            </span>
                        </div>
                        
                        <div class="mainContentCardBodyPost">
                            {!! $value->text !!}   
                        </div>
                        
                        <div class="mainContentCardFooterPost">
                            {!! $value->text !!}   
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            @endforeach
            <!-- START THE COMMENTS -->
            @foreach ($comments as $value)
                @php
                    $playerName = PlayerController::getPlayerInfo($value->userID, ['nome']);
                    if($value->userID == 0){
                        $roleName = 'BOT';
                        $roleImg = 'http://localhost/123/dono.png';
                        $roleColor = '#51B55C';
                    }else{
                        $adminInfo = AdminController::showAdminInfo($value->userID);
                        $roleInfo = PermissionController::getPerm($adminInfo->permID, ['name', 'color']);
                        $roleName = $roleInfo->name;
                        $roleImg = 'https://brasilnewstart.com.br/forum/uploads/monthly_2021_02/fundador.png.a0fd52da3037a16f15b784e9b0fbadc9.png';
                        $roleColor = $roleInfo->color;
                    }

                    $textCreated = HomeController::getDiffDate('now', $value->created_at);
                    if($value->created_at != $value->updated_at){
                        $textUpdated = HomeController::getDiffDate('now', $value->updated_at);
                    }else{
                        $textUpdated = null;
                    }
                    
                    $avatar = null;
                    $avatar = HomeController::getSkinBackground();
                    $randNumber = $avatar['rand'];
                    $type = $avatar['type'];
                    $images = $avatar['imageInfo'];

                @endphp
                @if ($value->hide==0)
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body cardBodyAll">
                            <div class="sideContentCardBodyPost">
                                <h5>
                                    <strong>
                                        <a href="https://brasilnewstart.com.br/forum/profile/84-giooo/?wr=eyJhcHAiOiJmb3J1bXMiLCJtb2R1bGUiOiJmb3J1bXMtY29tbWVudCIsImlkXzEiOjcyNDcsImlkXzIiOjMzMzk3fQ==">
                                            <span style="color:{{ $roleColor }} ;text-shadow: 0px 0px 2px {{ $roleColor }}">
                                                <img src="" alt="">
                                                <span>{{ $playerName->nome }}</span>
                                            </span>
                                        </a>
                                    </strong>
                                </h5>
                                <div class="product-img" style="margin-top: 20px">
                                    <img class="profile-user-img img-fluid img-circle p-0" style="background-image: url({{ $images[$type][$randNumber]['url'] }}); {{ $images[$type][$randNumber]['css'] }}" src="/img/avatars/{{ $playerInfo->skin }}" alt="User profile picture">
                                </div>
                                <h6 style="margin-top: 10px;">
                                    <span style="color:{{ $roleColor }} ;text-shadow: 0px 0px 2px {{ $roleColor }}">
                                        <img src="" alt="">
                                        <span>{{ $roleName }}</span>
                                    </span>
                                </h6>
                                <img style="max-width: 100%" src="{{ $roleImg }}" alt="" class="cAuthorGroupIcon">
                                <span style="color: #B7B7B7">{{ '0' /* UserInfo::externalUser($value->userID)->posts */ }} posts</span>
                            </div>
                        
                            <div class="mainContentCardHeaderPost">
                                <span>Postado {{ $textCreated }}</span>
                                @if($textUpdated != null)}
                                    <span><span class="ml-2 mr-2">•</span> Editado {{ $textUpdated }}</span>
                                @endif
                                <span class="float-right">
                                    <strong>IP:</strong> {{ $value->ip_address }}
                                    <span><span class="ml-2 mr-2">•</span> <i class="fas fa-share"></i></span>
                                    <span><span class="ml-2 mr-2">•</span> <i class="fas fa-flag"></i></span>
                                </span>
                            </div>
                            
                            <div class="mainContentCardBodyPost">
                                {!! $value->text !!}
                            </div>

                            <div class="footerCardBodyPost">
                                <a href="#">Citar</a>
                                <a href="#">Editar</a>
                                <div class="btn-group">
                                    <a type="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opções
                                    </a>
                                    <ul class="dropdown-menu" style="">
                                    <li><a class="dropdown-item" href="#">Dropdown link</a></li>
                                    <li><a class="dropdown-item" href="#">Dropdown link</a></li>
                                    </ul>
                                </div>
                            </div>
                        
                            <div class="mainContentCardFooterPost" align="center">
                                <hr style="background-color: #333847" />
                            </div>
                        </div>
                    </div>
                @elseif ($value->hide==1)
                    @php
                        /*
                        <div class="card card-outline card-{{ $color }}">
                            <div class="card-header">
                                <h3 class="card-title"><i class="{{ $icon }} mr-2 fa-xs"></i>{{ $playerName->nome }}</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool text-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                {!! $value->text !!}
                            </div>
                            <!-- /.card-body -->
                        </div>*/
                    @endphp
                @endif

            @endforeach
            <!-- /.card -->
            @if ($info->locked==0 && $info->status==0)
                <div class="card card-outline">
                    <!-- /.card-header -->
                    <form action="/comment/new/{{ AREA_ID }}/{{ $info->token }}" method="post">
                        @csrf
                        <div class="card-body p-0">
                            <div class="input-group">
                                <div class="col-12">
                                    <textarea id="summernote" style="display: none;" rows="20" name="text"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Enviar</button>
                        </div>
                    </form>
                    <!-- /.card-body -->
                </div>
            @endif

        </div>
    </div>
@stop

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $('#summernote').summernote({
          placeholder: 'Conte aqui com detalhes todo o ocorrido.',
          tabsize: 2,
          height: 150
        });
    </script>
@stop
