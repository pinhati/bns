@php
    use App\Http\Controllers\Server\PlayerController;
    use App\Http\Controllers\HomeController;
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard - Brasil New StarT')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
  <style>
    .info-box-text{
        margin-bottom: -7px !important;
    }
    .cardpost{
        margin-bottom: 0.5rem !important;
        border-radius: 0.5rem !important;
    }

.field-reactions:checked:focus ~ .text-desc, .field-reactions, [class*=reaction-], .text-desc {
  clip: rect(1px, 1px, 1px, 1px);
  overflow: hidden;
  position: absolute;
  top: 0;
  left: 0;
}

.field-reactions:checked ~ [class*=reaction-], .box-tohover:hover [class*=reaction-], .field-reactions:focus ~ .text-desc {
  clip: auto;
  overflow: visible;
  opacity: 1;
}

.main-title {
  background: #3a5795;
  padding: 10px;
  color: #fff;
  text-align: center;
  font-size: 16px;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
}

.text-desc {
  font-weight: normal;
  text-align: center;
  transform: translateY(-50px);
  white-space: nowrap;
  font-size: 13px;
  width: 100%;
}

[class*=reaction-] {
  border: none;
  background-image: url(http://deividmarques.github.io/facebook-reactions-css/assets/images/facebook-reactions.png);
  background-color: transparent;
  display: block;
  cursor: pointer;
  height: 48px;
  width: 48px;
  z-index: 11;
  transform-origin: 50% 100%;
  transform: scale(0.1);
  transition: all 0.3s;
  outline: none;
  will-change: transform;
  opacity: 0;
}

.box {
  position: absolute;
}

.field-reactions:focus ~ .label-reactions {
  border-color: rgba(88, 144, 255, 0.3);
}
.field-reactions:checked:focus ~ .label-reactions {
  border-color: transparent;
}
.toolbox {
  background: #0E0F14;
  height: 52px;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.08), 0 2px 2px rgba(0, 0, 0, 0.15);
  width: 300px;
  border-radius: 40px;
  top: -50px;
  left: 0;
  position: absolute;
  visibility: hidden;
  opacity: 0;
  transition: opacity 0.15s;
}

.box-tohover:hover [class*=reaction-] {
  transform: scale(0.8) translateY(-4px);
}
.box-tohover:hover [class*=reaction-]:hover, .box-tohover:hover [class*=reaction-]:focus {
  transition: all 0.2s ease-in;
  transform: scale(1) translateY(-4px);
}
.box-tohover:hover [class*=reaction-]:hover .legend-reaction, .box-tohover:hover [class*=reaction-]:focus .legend-reaction {
  opacity: 1;
}
.box-tohover:hover .toolbox {
  opacity: 1;
}
.box-tohover:hover .toolbox {
  visibility: visible;
}
.box-tohover:hover .reaction-love {
  transition-delay: 0.06s;
}
.box-tohover:hover .reaction-haha {
  transition-delay: 0.09s;
}
.box-tohover:hover .reaction-wow {
  transition-delay: 0.12s;
}
.box-tohover:hover .reaction-sad {
  transition-delay: 0.15s;
}
.box-tohover:hover .reaction-angry {
  transition-delay: 0.18s;
}

.field-reactions:checked ~ [class*=reaction-] {
  transform: scale(0.8) translateY(-4px);
}
.field-reactions:checked ~ [class*=reaction-]:hover, .field-reactions:checked ~ [class*=reaction-]:focus {
  transition: all 0.2s ease-in;
  transform: scale(1) translateY(-4px);
}
.field-reactions:checked ~ [class*=reaction-]:hover .legend-reaction, .field-reactions:checked ~ [class*=reaction-]:focus .legend-reaction {
  opacity: 1;
}
.field-reactions:checked ~ .toolbox {
  opacity: 1;
}
.field-reactions:checked ~ .toolbox,
.field-reactions:checked ~ .overlay {
  visibility: visible;
}
.field-reactions:checked ~ .reaction-love {
  transition-delay: 0.03s;
}
.field-reactions:checked ~ .reaction-haha {
  transition-delay: 0.09s;
}
.field-reactions:checked ~ .reaction-wow {
  transition-delay: 0.12s;
}
.field-reactions:checked ~ .reaction-sad {
  transition-delay: 0.15s;
}
.field-reactions:checked ~ .reaction-angry {
  transition-delay: 0.18s;
}

.reaction-like {
  left: 0;
  background-position: 0 -144px;
}

.reaction-love {
  background-position: -48px 0;
  left: 50px;
}

.reaction-haha {
  background-position: -96px 0;
  left: 100px;
}

.reaction-wow {
  background-position: -144px 0;
  left: 150px;
}

.reaction-sad {
  background-position: -192px 0;
  left: 200px;
}

.reaction-angry {
  background-position: -240px 0;
  left: 250px;
}
.like-emo > span{
    display: inline-block;
    width: 16px;
    height: 16px;
    border-radius: 50%;

}
.all-reactions > span{
    display: inline-block;
    width: 16px;
    height: 16px;
    border-radius: 50%;

}

.like-btn-gostei{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  background-position: -17px -151px;
  padding-left: 15px;
}

.like-btn-amei{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  background-position: 0 -168px;
}

.like-btn-haha{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  background-position: 0 -151px;
}

.like-btn-uau{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  background-position: -17px -185px;
}

.like-btn-triste{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  background-position: -17px -168px;
}

.like-btn-grr{
  background-image: url('{{ url("/images/reaction-small.png") }}');
  margin-top: 2px;
  background-position: -17px -117px;
}

.like-btn-text-gostei {
  color:rgb(88, 144, 255);
}
.like-btn-text-uau,.like-btn-text-haha,.like-btn-text-triste {
  color:rgb(240, 186, 21)
}
.like-btn-text-amei{
  color:rgb(242, 82, 104)
}
.like-btn-text-grr{
  color:rgb(247, 113, 75);
}
  </style>
  <div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Jogadores</span>
          <span class="info-box-number" id="playersInfo">Carregando...</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-green elevation-1"><i class="fas fa-dollar-sign"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Economia</span>
          <span class="info-box-number">$ {{ number_format($economy->money,0,'.','.') }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <!-- fix for small devices only -->
    <div class="clearfix hidden-md-up"></div>
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-coins"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Ouros</span>
          <span class="info-box-number">{{ number_format($economy->gold,0,'.','.') }} barras</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-info elevation-1"><i class="fab fa-sm fa-cuttlefish"></i><i class="fas fa-dollar-sign"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Cash's</span>
          <span class="info-box-number">C$ {{ number_format($economy->cash,0,'.','.') }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <div class="row">
    <div class="col-md-9">
        @foreach($feed as $key => $value)
          @php
            $checkReact = HomeController::checkIfReact($value->id);
          @endphp
          <div class="card cardpost" id="postID-{{ $value->id }}">
            <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane">
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="https://adminlte.io/themes/v3/dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                            <a href="#" class="text-white">{{ PlayerController::getPlayerInfo($value->userID, ['nome'])->nome }}</a>
                        </span>
                        <span class="description"><i class="fas fa-globe-americas mr-1"></i> Público - {{ $value->created_at }}</span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                        {{ $value->description }}
                      </p>

                      <p>
                        <div class="box">
                          <div class="box-tohover">
                            @if ($checkReact)
                                <span class="like-emo"> <!-- like emotions container -->
                                    <span class="like-btn-{{ strtolower($checkReact->reactionName) }}"></span> <!-- given emotions like, wow, sad (default:Like) -->
                                </span>
                                <a href="#" class="like-btn-text like-btn-text-{{ strtolower($checkReact->reactionName).' active' }}">{{ $checkReact->reactionName }}</a>
                            @else
                                <span class="like-emo"> <!-- like emotions container -->
                                    <span class="like-btn-gostei"></span> <!-- given emotions like, wow, sad (default:Like) -->
                                </span>
                                <a href="#" class="link-black like-btn-text">Gostei</a>
                            @endif
                            <div class="toolbox" data-postID="{{ $value->id }}">
                              <button class="reaction reaction-like" data-reaction="Gostei"></button>
                              <button class="reaction reaction-love" data-reaction="Amei"></button>
                              <button class="reaction reaction-haha" data-reaction="Haha"></button>
                              <button class="reaction reaction-wow" data-reaction="Uau"></button>
                              <button class="reaction reaction-sad" data-reaction="Triste"></button>
                              <button class="reaction reaction-angry" data-reaction="Grr"></button>
                            </div>
                          </div>
                          <p>
                            <span class="all-reactions"> <!-- like emotions container -->
                                @foreach (HomeController::getReactionsFeed($value->id) as $keyReactions => $valueReactions)
                                    <span class="like-btn-{{ strtolower($valueReactions->reactionName) }}"></span>
                                @endforeach
                            </span>
                            <a class="text-white msg-reaction" href="#">{{ $value->reactionsCount }}</a>
                          </p>
                        </div>
                        <span class="float-right">
                          <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comentários (5)
                          </a>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer card-comments">
                  <div class="card-comment">
                    <!-- User image -->
                    <img class="img-circle img-sm" src="https://adminlte.io/themes/v3/dist/img/user3-128x128.jpg" alt="User Image">

                    <div class="comment-text">
                      <span class="username">
                        Tenii
                        <span class="text-muted float-right">8:03 PM Today</span>
                      </span><!-- /.username -->
                      concordo
                    </div>
                    <!-- /.comment-text -->
                  </div>
                  <!-- /.card-comment -->
                </div>
                <div class="card-footer">
                  <form action="#" method="post">
                    <img class="img-fluid img-circle img-sm" src="https://adminlte.io/themes/v3/dist/img/user4-128x128.jpg" alt="Alt Text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
                    </div>
                  </form>
                </div>
          </div>
        @endforeach
    </div>
    <div class="col-3">
        <div class="card">
        <div class="card-header border-0">
          <h3 class="card-title">Jogador</h3>
        </div>
        <div class="card-body">
          <center>
            <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Skin_{{ $skin }}.png" width="147">
          </center>
          <!-- /.d-flex -->
        </div>
      </div>
      <div class="card">
        <div class="card-header border-0">
          <h3 class="card-title">Staff Online</h3>
        </div>
        <div class="card-body">
          <ul class="products-list product-list-in-card pl-2 pr-2">
            @foreach ($admins as $value)
                <li class="item">
                    <div class="product-img">
                        <img src="http://painel.brasilnewstart.com.br/dist/img/Skins/Avatar_211.png" alt="Product Image" class="img-size-50 direct-chat-img">
                    </div>
                    <div class="product-info">
                        <span style="color:#00C7E2 ;text-shadow: 0px 0px 2px #00C7E2;font-size: 20px;">
                        {{ $value->username }}
                        </span>
                        <span class="product-description mt-n1">
                            <span style="color:#00C7E2 ;text-shadow: 0px 0px 2px #00C7E2">Dono</span>
                        </span>
                    </div>
                </li>
            @endforeach

            <!-- /.item -->
          </ul>
          <!-- /.d-flex -->
        </div>
      </div>
    </div>
  </div>
@stop

@section('js')
  <script>

    $(document).ready(function(){
      $(".reaction").on("click",function(){   // like click
        data_reaction = $(this).attr("data-reaction");
        dataPostID = $(this).parent().attr("data-postID");
        $(`#postID-${dataPostID} .like-btn-emo`).removeClass().addClass('like-btn-emo').addClass('like-btn-'+data_reaction.toLowerCase());
        $(`#postID-${dataPostID} .like-btn-text`).text(data_reaction).removeClass().addClass('like-btn-text').addClass('like-btn-text-'+data_reaction.toLowerCase()).addClass("active");;

        if(data_reaction == "Gostei"){
            $(`#postID-${dataPostID} .like-emo`).html('<span class="like-btn-gostei"></span>');
        }else{
            $(`#postID-${dataPostID} .like-emo`).html('<span class="like-btn-'+data_reaction.toLowerCase()+'"></span>');
        }

        $.ajax({
          url: '/feed/addReaction',
          type: "post",
          data: {
            '_token': '{{ csrf_token() }}',
            'postID':dataPostID,
            'reactionName':data_reaction
          },
          dataType: 'JSON',
          success: function (returnValue) {
            quantityWithoutMe = (returnValue.quantity - 1);
            quantityWithMe = returnValue.quantity;
            if(quantityWithoutMe == 0){
                html = quantityWithMe;
            }else if(quantityWithoutMe > 1){
                html = `Você e outras ${quantityWithoutMe} pessoas`;
            }else{
                html = `Você e ${quantityWithoutMe} outra pessoa`
            }
            $(`#postID-${dataPostID} .msg-reaction`).html(html);
            allReactions = $(`#postID-${dataPostID} .all-reactions`).html("");
            allReactions = $(`#postID-${dataPostID} .all-reactions`);
            for(var k in returnValue.reactions) {
                allReactions.append(`<span class="like-btn-${returnValue.reactions[k].reactionName.toLowerCase()}"></span>`);
            }
          },
          error: function(returnValue){
            console.log(returnValue)
          }
        });
      });

      $(".like-btn-text").on("click",function(){ // undo like click
        if($(this).hasClass("active")){
          $(".like-btn-text").text("Gostei").removeClass().addClass('link-black').addClass('like-btn-text');
          $(".like-btn-emo").removeClass().addClass('like-btn-emo').addClass("like-btn-default");
          $(".like-emo").html('<span class="like-btn-gostei"></span>');
          $(".like-details").html("Arkaprava Majumder and 1k others");
        }
      });
    });
  </script>
@endsection
