<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Panel\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getPerm($permID, $info){
        $perm = Permission::where('id', $permID)->first($info);
        return $perm;
    }

    public static function checkPermIn($permID){
        $perm = Permission::whereIn('id', $permID)->get();
        return $perm;
    }

    public static function searchInArray($array, $valueInfo){
        foreach($array as $value){
            if($value[$valueInfo] == true){
                return true;
                break;
            }
        }
        return false;
    }
}
