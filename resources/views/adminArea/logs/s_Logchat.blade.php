@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Administrativo - LOG's</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12" id="col1">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-info mr-3"></i>Log Chat</h3>
                <div class="card-tools">
                    <a type="button" class="btn btn-tool" data-toggle="modal" href="#filter">
                        <i class="fas fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm" id="tbl_logChat">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Jogador</th>
                            <th scope="col">Texto</th>
                            <th scope="col">Pos. X</th>
                            <th scope="col">Pos. Y</th>
                            <th scope="col">Pos. Z</th>
                            <th scope="col" width="200">Data</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                   <!-- <tbody>
                        @foreach($log as $key => $value)
                            <tr>
                                <td scope="row">{{ $value->id }}</td>
                                <td title="PID: {{ $value->player_id }}">{{ $value->nome }}</td>
                                <td>{{ $value->texto }}</td>
                                <td>{{ $value->pos_x }}</td>
                                <td>{{ $value->pos_y }}</td>
                                <td>{{ $value->pos_z }}</td>
                                <td>{{ $value->datahora }}</td>
                                <td><div class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Ações
                                    </button>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item text-sm" href="/admin/logchat/{{ $value->pos_x }}/{{ $value->pos_y }}/{{ date('Y-m-d H:', strtotime($value->datahora)) }}">Conversas em raio de 100m</a>
                                      <a class="dropdown-item text-sm" href="http://localhost/pain/map.php?x={{ $value->pos_x }}&y={{ $value->pos_y }}">Visualizar no mapa</a>
                                    </div>
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody> -->
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="filter" style="display: none;" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <form method="post" action="/admin/logchat">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filtrar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Coluna</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Operador</th>
                                <th scope="col">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php

                                $structure = DB::connection('bnsCon')->select('describe log_chat');

                            @endphp
                            @foreach($structure as $key => $value)
                                <tr>
                                    <input type="hidden" value="{{ $value->Field }}" name="col[{{ $key }}]">
                                    @if($value->Key)
                                        @if($value->Extra == 'auto_increment')
                                            <td>{{ $value->Field }}<i class="fas fa-key text-warning ml-2" title="PRIMARY KEY"></i></td>
                                        @else
                                            <td>{{ $value->Field }}<i class="fas fa-key text-gray ml-2" title="ÍNDICE"></i></td>
                                        @endif
                                    @else
                                        <td>{{ $value->Field }}</td>
                                    @endif
                                    <td>{{ $value->Type }}</td>
                                    <td>
                                        <select style="border: none !important;" id="ColumnOperator{{ $key }}" class="form-control" name="operator[{{ $key }}]" width="100%">
                                            <option value="=">Igual (=)</option>
                                            <option value=">">Maior (>)</option>
                                            <option value=">=">Maior ou igual (&gt;=)</option>
                                            <option value="<">Menor (&lt;)</option>
                                            <option value="<=">Menor ou igual (&lt;=)</option>
                                            <option value="!=">Diferente (!=)</option>
                                            <option value="LIKE" @if (strpos($value->Type, 'varchar') !== false) {{ 'selected' }} @endif>Contém (LIKE)</option>
                                            <option value="NOT LIKE">Não Contém (NOT LIKE)</option>
                                        </select>
                                    </td>
                                    <td><input type="text" name="criterio[{{ $key }}]" class="form-control" style="width: 100%; border: none !important;" value=""></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script>
    //ajax populate with data
    $(document).ready(function() {
        var table = $('#tbl_logChat').DataTable({
            "aaSorting": [
                [0, 'desc'],
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
            },
            "searching": false,
            
            processing: true,
            serverSide: true,
            rowId: '#',
            ajax: {
                url: '/admin/logchat/data?{!! http_build_query(app("request")->all()) !!}',
                type: 'get'
            },
            columns: [
                { data: 'id', name: 'log_chat.id' },
                { data: 'nome', name: 'player.nome' },
                { data: 'texto', name: 'log_chat.texto' },
                { data: 'pos_x', name: 'log_chat.pos_x' },
                { data: 'pos_y', name: 'log_chat.pos_y' },
                { data: 'pos_z', name: 'log_chat.pos_z' },
                { data: 'datahora', name: 'log_chat.datahora' },  
                {
                    searchable: 'false',
                    render: function (data, type, row, meta) {
                    var pos_y = row['pos_y'];
                    var pos_x = row['pos_x'];
                    var pos_z = row['pos_z'];
                    var datetime = row['datahora'];
                    var dateUnix = new Date(Date.parse(datetime)).getTime() / 1000;

                    return '<div class="input-group-prepend"> <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Ações </button> <div class="dropdown-menu"> <a class="dropdown-item text-sm" href="/admin/logchat?coordinatesX='+pos_x+'&coordinatesY='+pos_y+'&coordinatesDate='+dateUnix+'">Conversas em raio de 100m</a> <a class="dropdown-item text-sm" :href="http://localhost/pain/map.php?x='+pos_x+'&y='+pos_y+'">Visualizar no mapa</a> </div> </div>'
                    },
                },   
            ]
        });
    })
</script>
@stop

