<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuggestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(){
        $areas = [];
        $areas[0] = DB::table('areas')->where('type', 0)->get(['name', 'id']);
        return view('suggestions.form')->with('areas', $areas);
    }
}
